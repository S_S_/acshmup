# Stage Files  
Stage files are JSON objects containing all of the details that need to be loaded for a stage. This includes:

- Name
- Description
- Music (Stage/Boss)  
- Script (Events)  

The stage script is an array of strings to be parsed and executed. Each line is one command, formatted as follows:  

- Time (Time in seconds into the stage's run)  
- Command (What to do)  
- Args (Any relevant parameters for the command)  
