#include "player.h"

extern App app;
extern Config config;

static void formation_front(Option *o, int index)
{
    // An evenly-spaced forward formation.
    int n_options = app.run.n_options;
    // Safety check:
    if (index < 0 || index >= n_options || n_options < 1) return;
    // Space the options evenly in a wide but shallow arc behind the player.
    float max_dist_side = 80; //o->parent->w*5;
    float dist_forward = o->parent->h*5;
    float x_line = 0;
    if (n_options > 1)
    {
        float ratio = (float)n_options/(app.run.options_max);
        float inner_ratio = (float)index/(n_options-1);
        // Distribute the options evenly on the X-axis. Define a length along which to distribute and then place.
        x_line = (max_dist_side)*((inner_ratio-0.5)*ratio);
        dist_forward = sin((inner_ratio)*PI)*dist_forward;
    }
    o->x = o->parent->x + x_line;
    // For now, simply place all options evenly behind the player.
    o->y = o->parent->y - dist_forward;

    return;
}

static void formation_rear(Option *o, int index)
{
    // An evenly-spaced rear formation.
    int n_options = app.run.n_options;
    // Safety check:
    if (index < 0 || index >= n_options || n_options < 1) return;
    // Space the options evenly in a wide but shallow arc behind the player.
    float max_dist_side = 120; //o->parent->w*5;
    float dist_back = o->parent->h*7;
    float x_line = 0;
    if (n_options > 1)
    {
        float ratio = (float)n_options/(app.run.options_max);
        float inner_ratio = (float)index/(n_options-1);
        // Distribute the options evenly on the X-axis. Define a length along which to distribute and then place.
        x_line = (max_dist_side)*((inner_ratio-0.5)*ratio);
        dist_back = sin((inner_ratio)*PI)*dist_back;
    }
    o->x = o->parent->x + x_line;
    // For now, simply place all options evenly behind the player.
    o->y = o->parent->y + dist_back;

    return;
}

f_func get_formation_function(enum formation f)
{
    switch (f)
    {
        case FORMATION_FRONT:
            return &formation_front;
        case FORMATION_REAR:
            return &formation_rear;
        default:
            return &formation_rear;
            break;
    }
}


/* Bombing. Get the type of bomb and activate it, starting from the entity. */
#define BOMB_DURATION 5
void launch_bomb(Entity *e)
{
    app.control_state[P_CONTROL_BOMB] = 0; // Only bomb once per press.
    int cost = MAX_POWER/app.run.options_max; // Spend one option per bomb.
    if (app.run.power < cost){
        // TODO: Play sad noise.
        return;
    }
    app.run.power -= cost;
    app.run.n_options -= 1;
    switch (app.run.bomb)
    {
        case BOMB_BASIC:
            e->invuln_ticks = BOMB_DURATION * FPS;
        default:
            break;
    }
    return;
}
