#include "input.h"

// TODO: Refactor to use player-bound keys (by keycode, not scancode).
extern App app;
extern Config config;

static void do_key_down(SDL_KeyboardEvent*);
static void do_key_up(SDL_KeyboardEvent*);
static void do_window_event(SDL_WindowEvent*);

void handleInput(void)
{
   SDL_Event event;
   // Handle the full event queue.
   while (SDL_PollEvent(&event)) 
   {
       switch (event.type)
       {
           case SDL_QUIT:
               exit(0);
               break;
           case SDL_KEYDOWN:
               do_key_down(&event.key);
               break;
           case SDL_KEYUP:
               do_key_up(&event.key);
               break;
           case SDL_WINDOWEVENT:
               do_window_event(&event.window);
               break;
           default:
               break;
       }
   }
}

static void do_key_up(SDL_KeyboardEvent *event)
{
    // Scancodes
	if (event->repeat == 0 && event->keysym.scancode < MAX_KEYBOARD_KEYS)
	{
        app.keyboard[event->keysym.scancode] = 0;
	}
    // Keycodes - Switches don't support key rebinding and keycodes have horrible numbers, so this is ugly.
    if (event->repeat == 0)
    {
        for (int i = 0 ; i < P_CONTROL_MAX ; ++i)
        {
            if (event->keysym.sym == app.controls[i])
                app.control_state[i] = 0;
        }
    }
}


static void do_key_down(SDL_KeyboardEvent *event)
{
    // Scancodes
	if (event->repeat == 0 && event->keysym.scancode < MAX_KEYBOARD_KEYS)
	{
        app.keyboard[event->keysym.scancode] = 1;
	}
    // Keycodes - Switches don't support key rebinding, so this is ugly.
    if (event->repeat == 0)
    {
        for (int i = 0 ; i < P_CONTROL_MAX ; ++i)
        {
            if (event->keysym.sym == app.controls[i])
                app.control_state[i] = 1;
        }
   }
}

static void do_window_event(SDL_WindowEvent *event)
{
    switch (event->event)
    {
        case SDL_WINDOWEVENT_RESIZED:
            if (event->data1 == SCREEN_WIDTH && event->data2 == SCREEN_HEIGHT)
            {
                GPU_UnsetViewport(app.screen);
                GPU_UnsetVirtualResolution(app.screen);
            }
            else
            {
                printf("Resized: New Size %dx%d\n", event->data1, event->data2);
                // Magic ratio by which to shove the viewport upward to get proper scaling.
                float y_adj = (((float)SCREEN_HEIGHT/event->data2)-1)*event->data2*!config.fullscreen;
                GPU_SetViewport(app.screen, GPU_MakeRect(0,y_adj,event->data1,event->data2));
                GPU_SetVirtualResolution(app.screen, SCREEN_WIDTH, SCREEN_HEIGHT);
            }
            // Either way, set the scaled co-ords for later access.
            app.scaled_coords.x = event->data1;
            app.scaled_coords.y = event->data2;
            break;
        default:
            break;
    }
}
