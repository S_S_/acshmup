#pragma once

#include "defs.h"

void set_bgm(const char *);

void init_sounds(void);

void cleanup_audio(void);

void play_sfx(enum sound_index, int);
