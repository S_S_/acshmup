#include "stage.h"
#include "draw.h"
#include "text.h"
#include "util.h"
#include "movements.h"
#include "patterns.h"
#include "player.h"
#include "sound.h"

extern App app;
extern Config config;
extern Stage stage;
extern Entity *player;
extern Mix_Chunk *sounds[SND_MAX];

extern ht *sprite_atlas;
extern uint32_t colour_shader, noise_shader, speen_shader;
extern GPU_ShaderBlock colour_block, noise_block, speen_block;
extern int colour_loc, noise_loc, speen_res_loc, speen_time_loc, speen_pos_loc, speen_rad_loc;

static void initPlayer(void);
static void new_run(int);
static void init_options(void);
static void logic(void);
static void draw(void);
static void load_stage_script(const char *);
static void free_stage_script(void);
static void free_entity(Entity*);
static int bulletHit(Entity*);
static int pickupCollect(Entity*);
static GPU_Image *stageBackdropTexture, *stageUITexture;
static int enemy_spawn_timer;
static int stage_reset_timer;
static atlasImage *pc_ship;

/* Clean the stage. */

static void resetStage(void)
{
    Entity *e;

    while (stage.shipHead.next)
    {
        e = stage.shipHead.next;
        stage.shipHead.next = e->next;
        free_entity(e);
    }
    while (stage.bulletHead.next)
    {
        e = stage.bulletHead.next;
        stage.bulletHead.next = e->next;
        free_entity(e);
    }
    while(stage.pickupHead.next)
    {
        e = stage.pickupHead.next;
        stage.pickupHead.next = e->next;
        free_entity(e);
    }

    free_stage_script(); // Clear the script before losing the reference.

    memset(&stage, 0, sizeof(Stage));
    stage.shipTail = &stage.shipHead;
    stage.bulletTail = &stage.bulletHead;
    stage.pickupTail = &stage.pickupHead;

    initPlayer();

    // Make RNG consistent per stage.
    srand(0xdeadbeef);

    enemy_spawn_timer = 0;

    // Start a new run. Remove this later.
    new_run(3);

    stage_reset_timer = FPS*2;
}

/* Return to a stage */
void gameplay_mode(void)
{
    Mix_ResumeMusic();
    //handleInput(); Process inputs properly.
    app.delegate.logic = logic;
    app.delegate.draw = draw;
}

/* Initialise a stage */

void initStage(const char *script)
{
    app.delegate.logic = logic;
    app.delegate.draw = draw;

    stageBackdropTexture = loadTexture("gfx/grey_backdrop.png");
    stageUITexture = loadTexture("gfx/ui_bg.png");
    pc_ship = (atlasImage*)ht_get(sprite_atlas, "gfx/prototype_ship.png");
    resetStage();
    // Load the stage details.
    //printf("Stage reset, fetching script.\n");
    load_stage_script(script);
    //resetStage();
}

static void new_run(int lives)
{
    memset(&app.run, 0, sizeof(Run));
    app.run.score = 0;
    app.run.graze = 0;
    app.run.lives = lives;
    app.run.power = 0;
    init_options();
}

static void initPlayer(void)
{
    player = calloc(1, sizeof(Entity));
    stage.shipTail->next = player;
    stage.shipTail = player;
    stage.timer = 0;

    player->side = SIDE_PLAYER;
    player->x = 640;
    player->y = 360;
    player->scale = 1;
    player->angle = 0;
    player->health = 3;
    player->speed = PLAYER_SPEED;
    player->reload = 4;
    player->texture = (atlasImage*)ht_get(sprite_atlas, "gfx/hitbox.png");
    player->movement = get_movement_function(M_FUNC_MOVE_PLAYER);
    player->w = player->texture->rect.w;
    player->h = player->texture->rect.h;
    player->drops.power = 4*MAX_POWER;
    player->hitbox.type = HITBOX_CIRCLE;
    player->hitbox.radius = 0.5*player->w*player->scale;
    //SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
    // Add an emitter.
    player->emitter = calloc(1, sizeof(Emitter));
    player->emitter->x_offset = 0;
    player->emitter->y_offset = -0.5*player->w*player->scale;
    player->emitter->pattern.period = 1;
    player->emitter->pattern.amplitude = 15;
    player->emitter->pattern.reload = player->reload;
    player->emitter->pattern.n_shots = 2;
    player->emitter->pattern.speed = BULLET_SPEED;
    player->emitter->pattern.t = player->t;
    player->emitter->pattern.sprite = "gfx/long_bullet.png";
    player->emitter->pattern.angle = 3*PI/2;
    player->emitter->pattern.shoot = get_pattern_function(P_FUNC_SHOOT_ROWS);
    player->emitter->pattern.sfx = SND_PLAYER_SHOT;
}

static void init_options(void)
{
    // Set the details for options based on the config parameters.
    app.run.primary = config.primary;
    app.run.focus = config.focus;
    app.run.bomb = config.bomb;
    // Universal player option details.
    app.run.option.side = SIDE_PLAYER;
    app.run.option.scale = 1;
    app.run.option.angle = 0;
    app.run.option.parent = player;
    // These won't be universal forever.
    app.run.options_max = 4;
    app.run.option_threshold = MAX_POWER/app.run.options_max;
    app.run.n_options = 0;
    // Spare some extensive text.
    Pattern p;
    p.t = stage.timer;
    p.reload = 2;
    switch (app.run.primary)
    {
        case PRIMARY_WIDE:
            app.run.option.primary_texture = (atlasImage*)ht_get(sprite_atlas, "gfx/option_aleph.png");
            app.run.option.primary_formation = get_formation_function(FORMATION_REAR);
            app.run.option.w = app.run.option.primary_texture->rect.w;
            app.run.option.h = app.run.option.primary_texture->rect.h;
            app.run.option.primary.x_offset = 0;
            app.run.option.primary.y_offset = 0.5*app.run.option.h;
            p.aimed = 0;
            p.period = 1;
            p.amplitude = PI/4;
            p.n_shots = 3;
            p.speed = BULLET_SPEED * 0.5;
            p.angle = 3*PI/2;
            p.shoot = get_pattern_function(P_FUNC_SHOOT_SPREAD);
            p.sprite = "gfx/basic_bullet.png";
            app.run.option.primary.pattern = p;
        default:
            break;
    }
    switch (app.run.focus)
    {
        case FOCUS_FORWARD:
            app.run.option.focus_texture = (atlasImage*)ht_get(sprite_atlas, "gfx/option_beth.png");
            app.run.option.focus_formation = get_formation_function(FORMATION_FRONT);
            app.run.option.focus.x_offset = 0;
            app.run.option.focus.y_offset = 0.5*app.run.option.h;
            p.aimed = 0;
            p.period = 3;
            p.amplitude = 15;
            p.n_shots = 2;
            p.speed = BULLET_SPEED * 0.75;
            p.angle = 3*PI/2;
            p.shoot = get_pattern_function(P_FUNC_SHOOT_ROWS);
            app.run.option.focus.pattern = p;
        default:
            break;
    }
    switch(app.run.bomb)
    {
        case BOMB_BASIC:
        default:
            break;
    }
}

static void doPlayer(void)
{
    if (player != NULL)
    {
        player->dx = player->dy = 0;
        //if (player->reload > 0)
        //    player->reload--;
        if (app.control_state[P_CONTROL_SHOOT] && player->emitter != NULL)
        {
            player->emitter->pattern.shoot(&player->emitter->pattern, \
                    player->x + player->emitter->x_offset, \
                    player->y + player->emitter->y_offset, player->side);
            play_sfx(SND_PLAYER_SHOT, CH_PLAYER_SHOOT);
        }
    }
}

// Handle options.
static void do_options(void){
    int new_option_count = app.run.power/app.run.option_threshold;
    if (player == NULL || !app.control_state[P_CONTROL_SHOOT])
        return;
    if (new_option_count > app.run.n_options){
        play_sfx(SND_POWERUP, CH_POWERUP);
    }
    app.run.n_options = new_option_count;
    for (int i = 0; i < app.run.n_options; ++i)
    {
        // Handle each option in turn, by index.
        if (!app.control_state[P_CONTROL_FOCUS])
        {
            app.run.option.primary_formation(&app.run.option, i);
            app.run.option.primary.pattern.shoot(&app.run.option.primary.pattern, \
                    app.run.option.x+app.run.option.primary.x_offset, \
                    app.run.option.y+app.run.option.primary.y_offset, \
                    app.run.option.side);

        } else {
            app.run.option.focus_formation(&app.run.option, i);
            app.run.option.focus.pattern.shoot(&app.run.option.focus.pattern, \
                    app.run.option.x+app.run.option.focus.x_offset,\
                    app.run.option.y+app.run.option.focus.y_offset,\
                    app.run.option.side);
        }
    }
}

static void free_entity(Entity *e)
{
    if (e->emitter != NULL)
        free(e->emitter);
    if (e->hitbox.type == HITBOX_POLYGON)
    {
        free(e->hitbox.x);
        free(e->hitbox.y);
    }
    free(e);
    return;
}

static Entity* make_pickup(enum pickup_type type, int val)
{
    Entity *e;
    e = calloc(1, sizeof(Entity));
    // Do type-dependent things.
    switch (type)
    {
        case PICKUP_POINT:
            e->side = PICKUP_POINT;
            e->texture = (atlasImage*)(ht_get(sprite_atlas, "gfx/point.png")); 
            break;
        case PICKUP_POWER:
            e->side = PICKUP_POWER;
            e->texture = (atlasImage*)(ht_get(sprite_atlas, "gfx/power.png"));
            break;
        case PICKUP_MULT:
            e->side = PICKUP_MULT;
            e->texture = (atlasImage*)(ht_get(sprite_atlas, "gfx/mult.png"));
            break;
        case PICKUP_LIFE:
            e->side = PICKUP_LIFE;
            break;
        default:
            break;
    }
    e->t = stage.timer;
    e->health = val;
    e->angle = 0;
    e->scale = max(log(val), 1);
    e->w = e->texture->rect.w;
    e->h = e->texture->rect.h;
    e->speed = DROP_SPEED;
    e->movement = get_movement_function(M_FUNC_MOVE_FALL);
    e->hitbox.type = HITBOX_CIRCLE; // We accept some inaccuracy because it doesn't matter.
    e->hitbox.radius = 0.5 * e->h;
    e->parent = player; // Let pickups home to the player. Clear when applicable.
                        // Add the pickup now, then return a pointer to it.
    e->dx = rand()%16 - 8;          // Maybe #define these at some point
    e->dy = (rand()%30)/10.f - 6;   // instead of leaving them as magic numbers.
    stage.pickupTail->next = e;
    stage.pickupTail = e;
    return e;
}

static void spawn_drops(Entity *e)
{
    // Creates all the drops for a given entity and sends them downwards on randomised arcs.
    Entity *p;
    // Don't drop anything if fully off-screen.
    if (e->x < BORDER_LEFT - e->w || e->x > BORDER_RIGHT || e->y < -e->h || e->y > SCREEN_HEIGHT)
        return;
    // Spawn all the drops in order.
    while (e->drops.point > 0)
    {
        if (e->drops.point >= 120){
            p = make_pickup(PICKUP_POINT, 120);
            e->drops.point -= 120;
        } else if (e->drops.point >= 5) {
            p = make_pickup(PICKUP_POINT, 5);
            e->drops.point -= 5;
        } else {
            p = make_pickup(PICKUP_POINT, 1);
            e->drops.point--;
        }
        p->y = e->y;
        p->x = e->x;
    }
    while (e->drops.power)
    {
        if (e->drops.power >= 120){
            p = make_pickup(PICKUP_POWER, 120);
            e->drops.power -= 120;
        } else if (e->drops.power >= 5) {
            p = make_pickup(PICKUP_POWER, 5);
            e->drops.power -= 5;
        } else {
            p = make_pickup(PICKUP_POWER, 1);
            e->drops.power--;
        }
        p->y = e->y;
        p->x = e->x;
    }
    while (e->drops.mult > 0)
    {
        if (e->drops.mult >= 120){
            p = make_pickup(PICKUP_MULT, 120);
            e->drops.mult -= 120;
        } else if (e->drops.point >= 5) {
            p = make_pickup(PICKUP_MULT, 5);
            e->drops.mult -= 5;
        } else {
            p = make_pickup(PICKUP_MULT, 1);
            e->drops.mult--;
        }
        p->y = e->y;
        p->x = e->x;
        --e->drops.mult;
    }
    return;
}

static void doShips(void)
{
    Entity *s, *prev;
    prev = &stage.shipHead;

    for (s = stage.shipHead.next ; s != NULL ; s = s->next)
    {
        s->movement(s);
        s->x += s->dx;
        s->y += s->dy;
        if (s != player && (s->x < BORDER_LEFT - s->w - OOB_THRESHOLD \
                    || s->x > BORDER_RIGHT + OOB_THRESHOLD \
                    || s->y < -s->h - OOB_THRESHOLD \
                    || s->y > SCREEN_HEIGHT + OOB_THRESHOLD))
        {
            s->health = 0;
        }
        // Handle deaths.
        if (s->health <= 0)
        {
            spawn_drops(s);
            if (s == player)
                player = NULL;
            if (s == stage.shipTail)
                stage.shipTail = prev;
            prev->next = s->next;
            free_entity(s);
            s = prev;
        }
        // Invuln ticks.
        if (s->invuln_ticks > 0)
            --s->invuln_ticks;

        prev = s;
    }
}

static void doBullets(void)
{
    Entity *b, *prev;
    prev = &stage.bulletHead;

    for (b = stage.bulletHead.next ; b != NULL ; b = b->next)
    {
        b->movement(b);
        b->x += b->dx;
        b->y += b->dy;
        if (bulletHit(b) || b->y < -b->h \
                || b->x < BORDER_LEFT-b->w - OOB_THRESHOLD \
                || b->x > BORDER_RIGHT + OOB_THRESHOLD \
                || b->y > SCREEN_HEIGHT + OOB_THRESHOLD)
        {
            if (b == stage.bulletTail)
                stage.bulletTail = prev;
            prev->next = b->next;
            free_entity(b);
            b = prev;
        }
        prev = b;
    }
}

static void doPickups(void)
{
    Entity *p, *prev;
    prev = &stage.pickupHead;

    for (p = stage.pickupHead.next ; p != NULL ; p = p->next){
        pickupCollect(p);
        p->movement(p);
        p->x += p->dx;
        p->y += p->dy;
        if (p->y < -p->h - 5 * OOB_THRESHOLD \
                || p->x < BORDER_LEFT -p->w - OOB_THRESHOLD \
                || p->x > BORDER_RIGHT + OOB_THRESHOLD \
                || p->y > SCREEN_HEIGHT + OOB_THRESHOLD)
        {
            p->health = 0;
        }
        if (p->health <= 0)
        {
            if (p == stage.pickupTail)
                stage.pickupTail = prev;
            prev->next = p->next;
            free_entity(p);
            p = prev;
        }
        prev = p;
    }
}

// Check each entity against the bullet.
static int bulletHit(Entity *b)
{
    Entity *s;

    for (s = stage.shipHead.next ; s != NULL ; s = s->next)
    {
        // Do grazing before other collision.
        if (s == player && s->side != b->side && !(b->grazed) && graze(s, b, GRAZE_RADIUS))
        {
            b->grazed = 1;
            ++app.run.graze;
        }

        if (s->side != b->side && collision(s, b))
        {
            if (s->side != SIDE_PLAYER)
            {
                app.run.score++;
            }
            b->health = 0;
            if (!s->invuln_ticks)
            {
                s->health--;
                // Special consequences of the player being hit: invuln, etc.
                if (s == player) {
                    s->invuln_ticks = 0.5*FPS;
                    play_sfx(SND_PLAYER_HIT, CH_PLAYER_DEATH);
                } else {
                    if (s->health > 10) { // Make this apply at some relative threshold.
                        play_sfx(SND_ENEMY_HEALTHY, CH_ENEMY_HIT);
                    } else {
                        play_sfx(SND_ENEMY_DAMAGED, CH_ENEMY_HIT);
                    }
                }
            }
            return 1;
        }
    }
    return 0;
}

// Check the pickup against the player, and set it to vacuum mode if required.
static int pickupCollect(Entity *p)
{
    Entity *s;

    for (s = stage.shipHead.next ; s != NULL ; s = s->next)
    {
        if (s != player)
            continue;
        // No grazing, but if the pickup is in the vacuum radius of the player, set it to vacuum mode.
        // Also do so for the border of auto-collection.
        if (graze(s, p, GRAZE_RADIUS*(1+2*app.control_state[P_CONTROL_FOCUS])) \
                || player->y <= 0.25*SCREEN_HEIGHT)
        {
            p->movement = get_movement_function(M_FUNC_MOVE_VACUUM);
        } 

        if (graze(s, p, GRAZE_RADIUS)){
            switch (p->side)
            {
                case PICKUP_POINT:
                    app.run.score += p->health;
                    break;
                case PICKUP_POWER:
                    app.run.power = min(app.run.power+5*p->health, MAX_POWER);
                    break;
                case PICKUP_MULT:
                    break;
                case PICKUP_LIFE:
                    ++player->health;
                    break;
                default:
                    break;
            }
            p->health = 0;
            play_sfx(SND_PICKUP, CH_PICKUP);
            return 1;
        }
    }

    return 0;
}

/* This is a testing function. It puts together enemies with random properties at a regular interval. */
static void spawnEnemies(void)
{
    Entity *enemy;
    if (--enemy_spawn_timer <= 0)
    {
        enemy = calloc(1, sizeof(Entity));
        stage.shipTail->next = enemy;
        stage.shipTail = enemy;

        enemy->t = stage.timer;
        enemy->side = SIDE_ENEMY;
        enemy->speed = 2 + (rand() % 4);
        enemy->scale = 0.5;
        enemy->angle = rand() % 360;
        enemy->health = 50;
        enemy->texture = (atlasImage*)(ht_get(sprite_atlas, "gfx/DDDemiurge.png"));
        enemy->reload = FPS * (1 + (rand() % 3));
        //SDL_QueryTexture(enemy->texture, NULL, NULL, &enemy->w, &enemy->h);
        enemy->w = enemy->texture->rect.w;
        enemy->h = enemy->texture->rect.h;
        enemy->x = rand()%(BORDER_RIGHT-BORDER_LEFT-(int)(enemy->w*enemy->scale)) \
                   + BORDER_LEFT + (int)(enemy->w*enemy->scale*0.5);
        enemy->y = 0-enemy->h;
        enemy->movement = get_movement_function(M_FUNC_MOVE_LINEAR);

        enemy->dy = 2 + (rand() % 4);

        enemy->drops.power = rand()%6;
        enemy->drops.point = rand()%2;
        enemy->drops.mult = rand()%3;

        enemy->hitbox.type = HITBOX_POLYGON;
        // Define a basic trapezium hitbox.
        float *hbx, *hby;
        hbx = malloc(4*sizeof(float));
        hby = malloc(4*sizeof(float));
        hbx[0] = (-enemy->w * cos(enemy->angle*PI/180) + enemy->h * sin(enemy->angle*PI/180))*enemy->scale/2;
        hby[0] = (-enemy->w * sin(enemy->angle*PI/180) - enemy->h * cos(enemy->angle*PI/180))*enemy->scale/2;
        hbx[1] = (enemy->w * cos(enemy->angle*PI/180) + enemy->h * sin(enemy->angle*PI/180))*enemy->scale/2;
        hby[1] = (enemy->w * sin(enemy->angle*PI/180) - enemy->h * cos(enemy->angle*PI/180))*enemy->scale/2;
        hbx[2] = (enemy->w/2.f * cos(enemy->angle*PI/180) - enemy->h * sin(enemy->angle*PI/180))*enemy->scale/2;
        hby[2] = (enemy->w/2.f * sin(enemy->angle*PI/180) + enemy->h * cos(enemy->angle*PI/180))*enemy->scale/2;
        hbx[3] = (-enemy->w/2.f * cos(enemy->angle*PI/180) - enemy->h * sin(enemy->angle*PI/180))*enemy->scale/2;
        hby[3] = (-enemy->w/2.f * sin(enemy->angle*PI/180) + enemy->h * cos(enemy->angle*PI/180))*enemy->scale/2;
        enemy->hitbox.n_vert = 4;
        enemy->hitbox.x = hbx;
        enemy->hitbox.y = hby;

        // Add an emitter.
        enemy->emitter = calloc(1, sizeof(Emitter));
        enemy->emitter->x_offset = 0;
        enemy->emitter->y_offset = 0.5*enemy->w*enemy->scale;
        enemy->emitter->pattern.period = 1;
        enemy->emitter->pattern.amplitude = 10;
        enemy->emitter->pattern.reload = enemy->reload;
        enemy->emitter->pattern.n_shots = rand() % 10 + 5;
        enemy->emitter->pattern.speed = ENEMY_BULLET_SPEED;
        enemy->emitter->pattern.t = enemy->t;
        enemy->emitter->pattern.sprite = "gfx/triangle.png";
        enemy->emitter->pattern.angle = PI/2;
        int shot = rand() % P_FUNC_MAX;
        enemy->emitter->pattern.shoot = get_pattern_function(shot);
        enemy->emitter->pattern.sfx = SND_ENEMY_SHOT;

        enemy_spawn_timer = 30+ (rand() % 60);
    }
}

static void doEnemies(void)
{
    Entity *e;

    for (e = stage.shipHead.next ; e != NULL ; e = e->next)
    {
        if (e != player && player != NULL)
        {
            if (e->emitter != NULL)
            {
                // Assume only one emitter, for now.
                e->emitter->pattern.shoot(&e->emitter->pattern, \
                        e->x + e->emitter->x_offset, e->y + e->emitter->y_offset, e->side);
            }
        }
    }
}

// Keep the player in-bounds. This could be merged into movement.
static void clipPlayer(void)
{
    if (player != NULL)
    {
        player->x = min(max(player->x, BORDER_LEFT + 0.5*player->w), BORDER_RIGHT - 0.5*player->w);
        player->y = min(max(player->y, 0 + 0.5*player->h), SCREEN_HEIGHT - 0.5*player->h);
    }
}

static void logic(void)
{
    ++stage.timer;
    doPlayer();
    do_options();
    //    doScript();
    doShips();
    doBullets();
    doPickups();
    spawnEnemies();
    doEnemies();
    clipPlayer();
    if (player == NULL && --stage_reset_timer <= 0)
    {
        resetStage();
        load_stage_script("stg/demo.json");
    }
}

/* Draw things */
static void drawPickups(void)
{
    Entity *p;
    for (p = stage.pickupHead.next ; p != NULL ; p = p->next)
    {
        blit_atlas_image(p->texture, p->x, p->y, p->angle, p->scale);
        if (DEBUG)
            drawHitbox(p);
    }
}

static void drawShips(void)
{
    Entity *s;
    for (s = stage.shipHead.next ; s != NULL ; s = s->next)
    {
        blit_atlas_image(s->texture, s->x, s->y, s->angle, s->scale);
        if (DEBUG)
        {
            drawHitbox(s);
            if (s == player)
            {
                // Draw the grazebox.
                SDL_Color blue = {0x00, 0x00, 0xff, 0xff};
                GPU_Circle(app.screen, player->x, player->y, GRAZE_RADIUS, blue);
            }
        }
    }
}

static void draw_options(void)
{
    // Iterate over the number of options and draw each in formation.
    if (player == NULL || app.run.n_options <= 0) 
        return;
    for (int i = 0; i < app.run.n_options; ++i){
        if (!app.control_state[P_CONTROL_FOCUS])
        {
            app.run.option.primary_formation(&app.run.option, i);
            blit_atlas_image(app.run.option.primary_texture, \
                    app.run.option.x, app.run.option.y, \
                    app.run.option.angle, app.run.option.scale);
        } else {
            app.run.option.focus_formation(&app.run.option, i);
            blit_atlas_image(app.run.option.focus_texture,\
                    app.run.option.x, app.run.option.y,\
                    app.run.option.angle, app.run.option.scale);
        }
    }

}

static void drawBullets(void)
{
    Entity *b;
    // Apply a zany rainbow shader.
    float t = (float)stage.timer/FPS;
    GPU_ActivateShaderProgram(colour_shader, &colour_block);
    update_colour_shader((1+sin(t)/2), (1+sin(t+1))/2, (1+sin(t+2))/2, 1.0f, colour_loc);
    //GPU_ActivateShaderProgram(noise_shader, &noise_block);
    //update_perlin_noise_shader(stage.timer, noise_loc);
    for (b = stage.bulletHead.next ; b != NULL ; b = b->next)
    {
        blit_atlas_image(b->texture, b->x, b->y, b->angle*180/PI, b->scale);
        // Debug code to trace from hitbox centre to centre.
        /*SDL_Colour blue = {0, 0, 0xff, 0xff};
          if (player)
          GPU_Line(app.screen, player->x, player->y, b->x, b->y, blue);
          */
    }
    /* Drop the shader. */
    GPU_ActivateShaderProgram(0, NULL);
    if (DEBUG)
    {
        for (b = stage.bulletHead.next ; b != NULL ; b = b->next)
        {
            drawHitbox(b);
        }
    }
}

static void drawHUD(void)
{
    // A lot of this could be consolidated if required, or have fonts switched and so on.
    blit(stageUITexture, 0, 0);
    char graze[12], score[12], power[12], lives[12];
    snprintf(graze, 12, "%010d", app.run.graze);
    snprintf(score, 12, "%010d", app.run.score);
    snprintf(lives, 12, "%d", player != NULL ? player->health : 0);
    snprintf(power, 12, "%03d/%d", app.run.power, MAX_POWER);
    GPU_Image *r = getTextTexture(graze);
    GPU_Image *s = getTextTexture(score);
    GPU_Image *p = getTextTexture(power);
    GPU_Image *l = getTextTexture(lives);
    GPU_Image *t = getTextTexture("Wings of Clay");
    blit(t, 50, 50);
    GPU_Image *u = getTextTexture("Lives\n\nPower\n\nScore\n\nGraze");
    blit(u, 980, 50);
    blit(r, 980, 400);
    blit(s, 980, 300);
    blit(p, 980, 200);
    blit(l, 980, 100);
    GPU_FreeImage(r);
    GPU_FreeImage(s);
    GPU_FreeImage(t);
    GPU_FreeImage(u);
    GPU_FreeImage(l);
    GPU_FreeImage(p);
}

static void draw_player(void){
    if (player == NULL)
        return;
    // Draw a subtler glowing effect scaled to the graze radius, if focused.
    if (app.control_state[P_CONTROL_FOCUS])
    {
        SDL_Color soft_red = {0xaa, 0x44, 0x44, 0x88};
        // TODO: Add a fun shader.
        GPU_ActivateShaderProgram(speen_shader, &speen_block);
        update_speen_shader(app.scaled_coords.x, app.scaled_coords.y, speen_res_loc, stage.timer, speen_time_loc, player->x, player->y, speen_pos_loc, 3*GRAZE_RADIUS, speen_rad_loc);
        GPU_CircleFilled(app.screen, player->x, player->y, 3*GRAZE_RADIUS, soft_red);
        GPU_ActivateShaderProgram(0, NULL);
    }
    // Draw a glowing effect scaled to i-frames, if applicable.
    if (player->invuln_ticks > 0)
    {
        SDL_Color red = {0xff, 0, 0, 0xff};
        // TODO: add a shader to provide the desired effect.
        GPU_ActivateShaderProgram(colour_shader, &colour_block);
        GPU_CircleFilled(app.screen, player->x, player->y, log(player->invuln_ticks)*10, red);
        GPU_ActivateShaderProgram(0, NULL);
    }
    blit_atlas_image(pc_ship, player->x, player->y, 0, 1);
}

static void draw(void)
{
    blit(stageBackdropTexture, 0, 0);
    SDL_Color off_green = {0x44, 0xbb, 0x44, 0xff};
    GPU_RectangleFilled(app.screen, BORDER_LEFT, 0, BORDER_RIGHT, SCREEN_HEIGHT, off_green);
    GPU_ActivateShaderProgram(noise_shader, &noise_block);
    update_perlin_noise_shader((float)stage.timer/FPS, noise_loc);
    //GPU_ActivateShaderProgram(colour_shader, &colour_block);
    //float t = (float)stage.timer/FPS;
    //update_colour_shader((1+sin(t)/2), (1+sin(t+1))/2, (1+sin(t+2))/2, 1.0f, colour_loc);
    SDL_Color white = {0xff, 0xff, 0xff, 0xff};
    GPU_RectangleFilled(app.screen, BORDER_LEFT, 0, BORDER_RIGHT, SCREEN_HEIGHT, white);
    GPU_ActivateShaderProgram(0, NULL);
    // Draw the player beneath everything else.
    draw_player();
    draw_options();
    drawPickups();
    drawShips();
    drawBullets();
    drawHUD();
}

/* Process stage JSON */
static void free_stage_script(void)
{
    // Goes through the stage's script and releases whatever is required before zeroing the memory.
    if (!stage.script)
        return;
    for (int i = 0; i < stage.script->n_lines; ++i)
        free(stage.script->lines[i]);
    free(stage.script->lines);
    free(stage.script);
    return;
}

static void load_stage_script(const char *path)
{
    // Empty the stage, just in case.
    free_stage_script();
    // Parse the JSON.
    json_t *root, *data;
    json_error_t error;
    root = json_load_file(path, 0, &error);
    if (!root) // Error checking: root will be null on failure to load.
    {
        printf("Unable to load stage %s: %s at %s\n", path, error.text, error.source);
        exit(1); // We could handle errors more gracefully, but this will do for now.
    }
    if (!json_is_object(root))
    {
        printf("Stage file is not an object.\n");
        json_decref(root);
        exit(1); // As above.
    }
    // Read in the data.
    stage.name = json_string_value(json_object_get(root, "name"));
    stage.desc = json_string_value(json_object_get(root, "desc"));
    set_bgm(json_string_value(json_object_get(root, "music")));
    // The actual stage script.
    data = json_object_get(root, "script");
    if (!json_is_array(data))
    {
        printf("Script incorrectly formatted.");
        //json_decref(data);
        json_decref(root);
        exit(1);
    }
    // Allocate and set the script.
    stage.script = calloc(1, sizeof(Script));
    stage.script->n_lines = json_array_size(data);
    stage.script->curr_line = 0;
    // Allocate the lines array.
    stage.script->lines = calloc(stage.script->n_lines, sizeof(char *));
    for (int i = 0; i < stage.script->n_lines; ++i)
    {
        stage.script->lines[i] = malloc(MAX_LINE_LENGTH);
        strncpy(stage.script->lines[i], json_string_value(json_array_get(data, i)), MAX_LINE_LENGTH);
    }
    // Free and exit.
    //json_decref(data);
    json_decref(root);
    return;
}
