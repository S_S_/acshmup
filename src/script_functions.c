#include "script_functions.h"
#include "util.h"
#include "draw.h"
#include "movements.h"
#include "patterns.h"

extern Stage stage;
extern Config config;
extern ht *sprite_atlas;
extern m_func get_movement_function(int);

// Spawns an enemy and adds it to the stage's fighter list.
void spawn_enemy(int x, int y, int health, float scale, int speed, int move, int pattern, const char *sprite)
{   
    //extern GPU_Image *enemyTexture; // Placeholder.
    Entity *e;
    e = calloc(1, sizeof(Entity));
    // Add it to the list.
    stage.shipTail->next = e;
    stage.shipTail = e;

    // Initialise fields.
    e->t = stage.timer;
    e->side = SIDE_ENEMY;
    e->scale = scale;
    e->angle = 0;
    e->health = health;
    e->texture = (atlasImage*)(ht_get(sprite_atlas, sprite));
    e->x = x;
    e->y = y;
    if (e->texture == NULL){
        SDL_Log("Texture not found: %s.\n", sprite);
        e->w = 0;
        e->h = 0;
    } else {
        e->w = e->texture->rect.w;
        e->h = e->texture->rect.h;
    }
    e->reload = FPS * (1 + rand() % 3);
    e->dx = 0;
    e->dy = speed;
    e->movement = get_movement_function(move);
}

// Adds an emitter to the newest entity on the stack. Position is relative to the ship. Includes an optional sprite to be overlaid on the ship; use NULL otherwise.
// X and Y zero will centre the emitter beneath the sprite.
void add_ship_emitter(int x, int y, int pattern, const char *sprite)
{
    return;
}

// Adds a static emitter somewhere on the screen. Includes an optional sprite: use NULL otherwise.
void add_static_emitter(int x, int y, float delay, float duration, int pattern, const char *sprite)
{
    return;
}
