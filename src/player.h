// Include guard.
#pragma once

#include "defs.h"

// Player-specific defs that everything else really doesn't need.

f_func get_formation_function(enum formation);

void launch_bomb(Entity*);
