#include "init.h"
#include "util.h"
#include "draw.h"
#include "menus.h"
#include "settings.h"
#include "text.h"
#include "sound.h"

extern App app;
extern Config config;
extern atlasImage *sprites;
extern GPU_Image *atlas_texture;
extern ht *sprite_atlas;

// Initialise everything.
void init(const char *title)
{
    /* Get the basic config in place before anything else. */
    init_config();

    /* SDL_GPU */
    int SDL_flags = SDL_WINDOW_RESIZABLE | (config.fullscreen*SDL_WINDOW_FULLSCREEN_DESKTOP);

    app.screen = GPU_Init(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_flags);
    if (app.screen == NULL)
    {
        printf("Unable to initialise SDL_gpu renderer.\n");
        exit(1);
    }
    // Try to get a pointer to the underlying SDL window.
    uint32_t window_id = app.screen->renderer->current_context_target->context->windowID;
    app.window = SDL_GetWindowFromID(window_id);
    SDL_SetWindowTitle(app.window, title);
    SDL_ShowCursor(0);

    // Initialise TTF support.
    if (TTF_Init() < 0)
    {
        printf("Failed to initialise SDL TTF: %s\n", SDL_GetError());
        exit(1);
    }
    init_fonts();

    // Initialise audio.
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
    {
        printf("Failed to initialise SDL Mixer: %s\n", SDL_GetError());
        exit(1);
    }
    Mix_AllocateChannels(MAX_SND_CHANNELS);
    init_sounds();

    // Load the sprite atlas.
    init_atlas();

    // Init controls:
    for (int i = 0; i < P_CONTROL_MAX; ++i)
        app.controls[i] = config.controls[i];

    // Compile and load shaders.
    init_shaders();

    // Initialise menus and dump the programme in the main menu.
    init_menus();
}

// Tidy up everything on close.
void cleanup()
{
    // Free shaders.
    cleanup_shaders();
    // Exit out of TTF.
    TTF_Quit();
    // Exit out of audio.
    cleanup_audio();
    Mix_Quit();
    // Free textures.
    free(sprites);
    ht_destroy(sprite_atlas);
    GPU_FreeImage(atlas_texture);
    // Shut down SDL
    //SDL_Quit();
    GPU_Quit();
    // Free menus.
    cleanup_menus();
    printf("\nExiting.\n");
}
