#include "scripts.h"
#include "script_functions.h"

extern Config config;

/* Scripts ----
 * A basic system to support plaintext scripting to generate level events.
 * Generally, a format to do some script at some timestamp. This should ideally
 * be framerate-independent, calling the script at float n seconds.
 */

/* Seconds since the start of the stage. */
static float stage_time(void)
{
    // This might have to be more complex in the future.
    return (float)stage.timer / FPS;
}


/* Get a script from a file. */
Script *load_script(const char *path)
{
    Script *s;
    SDL_RWops *rwops;

    s = malloc(sizeof(Script));
    rwops = SDL_RWFromFile(path, "r");

    SDL_RWclose(rwops);
    return s;
}

/* Free a script object. */
void free_script(Script *s)
{
    if (s->lines != NULL)
    {
        for (int i = 0; i < s->n_lines; ++s)
        {
            free(s->lines[i]);
        }
    }
    free(s);
    return;
}

/* Compare the current stage time against the script, and all functions as applicable. */
void doScript(void)
{
    float now, script_time = 0;
    char script_line[MAX_LINE_LENGTH], command[30], args[MAX_LINE_LENGTH];
    now = stage_time();
    do //TODO: Ensure that this never iterates beyond the bounds in subsequent calls more cleanly.
    {
        if (stage.script->curr_line >= stage.script->n_lines)
            break;
        //printf("Line %d:\n", stage.script->curr_line);
        strncpy(script_line, stage.script->lines[stage.script->curr_line], MAX_LINE_LENGTH);
        sscanf(script_line, "%f %s %[^\t\n]", &script_time, command, args);
        // Don't execute if it's not yet time.
        if (script_time > now)
            continue;
        printf("Time: %f; Command: %s; Args: %s\n", script_time, command, args);
        // Giant brick of cases to handle script commands. Could be prettified with an enum and X-macro.
        if (strcmp(command, "SPAWN_ENEMY") == 0)
        {
            printf("Creating enemy.\n");
            int x, y, health, speed;
            float scale;
            char move[100], pattern[100], texture_name[100];
            sscanf(args, "%d %d %d %f %d %s %s %s", &x, &y, &health, &scale, &speed, move, pattern, texture_name);
            printf("X: %d Y: %d Health: %d Scale: %f Speed: %d Move: %s Pattern: %s, Sprite: %s\n",
                    x, y, health, scale, speed, move, pattern, texture_name);
            spawn_enemy(x, y, health, scale, speed, str_to_movement(move), 0, texture_name);
        } else if (strcmp(command, "SET_BGM") == 0)
        {
            char path[MAX_LINE_LENGTH];
            sscanf(args, "%s", path);
            SDL_Log("Setting BGM to %s.\n", path);
            set_bgm(path);
        }

        ++stage.script->curr_line;
    } while (script_time <= now && stage.script->curr_line < stage.script->n_lines);
    //if (now < 0.5)
    //    printf("It's early: %f seconds.\n", now);
    return;
}
