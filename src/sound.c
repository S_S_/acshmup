#include "sound.h"

extern App app;
extern Config config;
extern Mix_Chunk* sounds[SND_MAX];

/* Sets the current BGM to a specified track.
 * SDL has only one `music` track SND_MAX_CHANNEL chunks. As such, this always checks for an old BGM, frees it if applicable and then sets the requested one.
 * */
void set_bgm(const char *path)
{
    if (app.bgm != NULL)
    {
        Mix_HaltMusic();
        Mix_FreeMusic(app.bgm);
        app.bgm = NULL;
    }
    app.bgm = Mix_LoadMUS(path);
    Mix_PlayMusic(app.bgm, -1);
    return;
}

/* Load sounds here. Currently hard-coded.
 * */
static void load_sounds(void)
{
    sounds[SND_PLAYER_HIT] = Mix_LoadWAV("snd/sfx/explosion.wav");
    sounds[SND_ENEMY_HEALTHY] = Mix_LoadWAV("snd/sfx/hit_healthy.wav");
    sounds[SND_ENEMY_DAMAGED] = Mix_LoadWAV("snd/sfx/hit_damaged.wav");
    sounds[SND_PLAYER_SHOT] = Mix_LoadWAV("snd/sfx/player_shot.wav");
    sounds[SND_ENEMY_SHOT] = Mix_LoadWAV("snd/sfx/enemy_shot.wav");
    sounds[SND_DEATH_SMALL] = Mix_LoadWAV("snd/sfx/enemy_death_small.wav");
    sounds[SND_DEATH_LARGE] = Mix_LoadWAV("snd/sfx/enemy_death_large.wav");
    sounds[SND_PICKUP] = Mix_LoadWAV("snd/sfx/pickup.wav");
    sounds[SND_POWERUP] = Mix_LoadWAV("snd/sfx/powerup.wav");
    // Some chunk-specific audio adjusting.
    Mix_VolumeChunk(sounds[SND_ENEMY_HEALTHY], MIX_MAX_VOLUME/8);
    Mix_VolumeChunk(sounds[SND_ENEMY_DAMAGED], MIX_MAX_VOLUME/8);
    Mix_VolumeChunk(sounds[SND_ENEMY_SHOT], MIX_MAX_VOLUME/8);
    Mix_VolumeChunk(sounds[SND_PLAYER_SHOT], MIX_MAX_VOLUME/6);
    Mix_VolumeChunk(sounds[SND_PLAYER_HIT], MIX_MAX_VOLUME/4);
    Mix_VolumeChunk(sounds[SND_DEATH_SMALL], MIX_MAX_VOLUME/8);
    Mix_VolumeChunk(sounds[SND_DEATH_LARGE], MIX_MAX_VOLUME/8);
    Mix_VolumeChunk(sounds[SND_PICKUP], MIX_MAX_VOLUME/6);
    Mix_VolumeChunk(sounds[SND_POWERUP], MIX_MAX_VOLUME/3);
}

/* Called at programme's start. Sets the menu theme and ensures that everything is running.
 * */
void init_sounds(void)
{
    // BGM.
    Mix_VolumeMusic(config.vol_bgm);
    set_bgm("snd/bgm/menubach.ogg");

    // SFX.
    // For convenience, hard-code each channel to 0.5 vol, then give the user the master slider.
    for (int i = 0; i < MAX_SND_CHANNELS; ++i) {
        Mix_Volume(i, MIX_MAX_VOLUME/2);
    }
    memset(sounds, 0, sizeof(Mix_Chunk*) * SND_MAX);
    load_sounds();
}

// Free sounds and music.
void cleanup_audio(void)
{
    for (int i = 0; i < SND_MAX; ++i) {
        Mix_FreeChunk(sounds[i]);
    }
    Mix_FreeMusic(app.bgm);
}


// Play a sound chunk.
void play_sfx(enum sound_index sfx, int channel)
{
    if (sfx == SND_NONE) return;
    Mix_PlayChannel(channel, sounds[sfx], 0);
}
