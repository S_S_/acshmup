#pragma once

#include "defs.h"

void init_fonts(void);

GPU_Image *toTexture(SDL_Surface *surface, int destroySurface); //Frees the surface if the int is set.
GPU_Image *getTextTexture(const char *text);
