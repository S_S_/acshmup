#include "settings.h"
#include "sound.h"

extern App app;
extern Config config;
extern Mix_Chunk* sounds[SND_MAX];

// Initialise the game configuration, reading from a config file if available.
// If unavailable, tries to create a config file in `cfg`.
void init_config(void)
{
    // Load the config.
    json_t *config_file = load_config();
    if (config_file == NULL)
    {
        // Do things.
        puts("Config file not found or invalid. Creating default at cfg/config.json.");
        // Definitionally, our config is null, so we can overwrite it.
        config_file = json_object();
        json_object_set_new(config_file, "keybinds", \
                json_pack("[iiiiiiii]", SDLK_UP, SDLK_DOWN, SDLK_LEFT, SDLK_RIGHT, \
                    SDLK_z, SDLK_LSHIFT, SDLK_x, SDLK_ESCAPE));
        json_object_set_new(config_file, "fullscreen", json_false());
        json_object_set_new(config_file, "fps_max", json_integer(60));
        json_object_set_new(config_file, "vol_bgm", json_integer(100));
        json_object_set_new(config_file, "vol_sfx", json_integer(100));
        json_object_set_new(config_file, "primary", json_integer(PRIMARY_WIDE));
        json_object_set_new(config_file, "focus", json_integer(FOCUS_FORWARD));
        json_object_set_new(config_file, "bomb", json_integer(BOMB_BASIC));
        // Save the config file.
        FILE *fp = fopen("cfg/config.json", "wb");
        json_dumpf(config_file, fp, JSON_INDENT(2));
        fclose(fp);
        puts("Config file created.");
    }

    config.fullscreen = json_boolean_value(json_object_get(config_file, "fullscreen"));
    config.max_framerate = json_integer_value(json_object_get(config_file, "fps_max"));
    config.vol_bgm = json_integer_value(json_object_get(config_file, "vol_bgm"));
    config.vol_sfx = json_integer_value(json_object_get(config_file, "vol_sfx"));
    config.primary = json_integer_value(json_object_get(config_file, "primary"));
    config.focus = json_integer_value(json_object_get(config_file, "focus"));
    config.bomb = json_integer_value(json_object_get(config_file, "bomb"));
    printf("Fullscreen: %d\nFPS: %d\nBGM: %d\nSFX: %d\n", config.fullscreen, \
            config.max_framerate, config.vol_bgm, config.vol_sfx);
    // Read the controls.
    json_t *keys;
    keys = json_object_get(config_file, "keybinds");
    if (json_array_size(keys) != P_CONTROL_MAX)
    {
        puts("Error: wrong number of keybinds supplied.");
        printf("Config file: %zu\nExpected: %d\n", json_array_size(keys), P_CONTROL_MAX);
        exit(1);
    }
    else 
    {
        for (int i = 0; i < P_CONTROL_MAX; ++i)
        {
            config.controls[i] = json_integer_value(json_array_get(keys, i));
            printf("Controls %d: %s\n", i, SDL_GetKeyName(config.controls[i]));
        }
    }
    json_decref(keys);
    json_decref(config_file);
}

json_t* load_config(void)
{
    // Look for a config directory. This is just the local `.cfg`.
    // In case of future extension, consider allowing `%XDG_CONFIG_HOME%` or `%APPDATA%`.
    json_t *cfg;
    json_error_t error;
    cfg = json_load_file("cfg/config.json", 0, &error);
    // For now, since we're not yet lloking for alternative paths, just return.
    return cfg;
}

void update_config(void)
{
    // Update the application configuration. Start accounting for actual confdir at some stage.
        json_t *config_file = json_object();
        json_object_set_new(config_file, "keybinds", \
                json_pack("[iiiiiiii]", \
                    config.controls[P_CONTROL_UP], \
                    config.controls[P_CONTROL_DOWN], \
                    config.controls[P_CONTROL_LEFT], \
                    config.controls[P_CONTROL_RIGHT], \
                    config.controls[P_CONTROL_SHOOT], \
                    config.controls[P_CONTROL_FOCUS], \
                    config.controls[P_CONTROL_BOMB], \
                    config.controls[P_CONTROL_BACK]));
        json_object_set_new(config_file, "fullscreen", json_false());
        json_object_set_new(config_file, "fps_max", json_integer(config.max_framerate));
        json_object_set_new(config_file, "vol_bgm", json_integer(config.vol_bgm));
        json_object_set_new(config_file, "vol_sfx", json_integer(config.vol_sfx));
        json_object_set_new(config_file, "primary", json_integer(config.primary));
        json_object_set_new(config_file, "focus", json_integer(config.focus));
        json_object_set_new(config_file, "bomb", json_integer(config.bomb));
        // Save the config file.
        FILE *fp = fopen("cfg/config.json", "wb");
        json_dumpf(config_file, fp, JSON_INDENT(2));
        fclose(fp);
}

void reset_config(void)
{
    // In the laziest way possible, resets the config to the defaults:
    // Rather than refactoring, currently deletes it and then re-initialises.

    // First, check for the existing config file.
    FILE *file;
    if ((file = fopen("cfg/config.json", "r")))
    {
        fclose(file);
        if (remove("cfg/config.json") != 0)
            printf("Error removing old config.\n");
        init_config();
    }
}

// Volume:
// Set volume somewhere between 0 and 100, in the next increment of 5.
// Afterwards, set the SDL volume, appropriately scaled.

void inc_vol_bgm(void)
{
    config.vol_bgm = min(config.vol_bgm + 5, 100);
    Mix_VolumeMusic(MIX_MAX_VOLUME * config.vol_bgm/100.f);
    update_config();
}
    
void inc_vol_sfx(void)
{
    config.vol_sfx = min(config.vol_sfx + 5, 100);
    Mix_MasterVolume(MIX_MAX_VOLUME * config.vol_sfx/100.f);
    Mix_PlayChannel(-1, sounds[SND_PLAYER_HIT], 0);
    update_config();
}

void dec_vol_bgm(void)
{
    config.vol_bgm = max(config.vol_bgm - 5, 0);
    Mix_VolumeMusic(MIX_MAX_VOLUME * config.vol_bgm/100.f);
    update_config();
}

void dec_vol_sfx(void)
{
    config.vol_sfx = max(config.vol_sfx - 5, 0);
    Mix_MasterVolume(MIX_MAX_VOLUME * config.vol_sfx/100.f);
    play_sfx(SND_PLAYER_HIT, -1);
    update_config();
}

// Toggle fullscreen.
// Puts the programme in windowed mode if fullscreen and vice versa.
void toggle_fullscreen(void)
{
    config.fullscreen = !config.fullscreen;
    //GPU_FreeTarget(app.screen);
    GPU_Quit();
    int SDL_flags = SDL_WINDOW_RESIZABLE | config.fullscreen*SDL_WINDOW_FULLSCREEN_DESKTOP;
    //GPU_Target *new_screen = GPU_Init(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_flags);
    puts("Got here!");
    app.screen = GPU_Init(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_flags);
    if (app.screen == NULL)
    {
        printf("Unable to initialise SDL_gpu renderer.\n");
        exit(1);
    }
    update_config();
}
