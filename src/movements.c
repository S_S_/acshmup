#include "movements.h"
#include "util.h"
#include "player.h"
#include "menus.h"

extern App app;
extern Config config;
extern Stage stage;
extern menu pause_menu;

static void move_player(Entity *e)
{
    float net_move = e->speed - 0.5*app.control_state[P_CONTROL_FOCUS]*e->speed;
    if (app.control_state[P_CONTROL_UP])
        e->dy = -net_move;
    if (app.control_state[P_CONTROL_DOWN])
        e->dy = net_move;
    if (app.control_state[P_CONTROL_LEFT])
        e->dx = -net_move;
    if (app.control_state[P_CONTROL_RIGHT])
        e->dx = net_move;
    if (app.control_state[P_CONTROL_BOMB])
        launch_bomb(e);
    if (app.control_state[P_CONTROL_BACK])
    {
        // Unpress buttons on pause.
        memset(app.control_state, 0, P_CONTROL_MAX * sizeof(int));
        Mix_PauseMusic();
        menu_mode(&pause_menu);
    }
    return;
}

static void move_down(Entity *e)
{
    // Set move downward. Ideally then optimise out of calling this again.
    e->dx = 0;
    e->dy = e->speed;
    e->movement =get_movement_function(M_FUNC_MOVE_LINEAR);
    return;
}

static void move_direction(Entity *e)
{
    // Set move in the specified linear direction, doing a basic transform of the speed to fit the angle.
    e->dx = e->speed * cos(e->angle);
    e->dy = e->speed * sin(e->angle);
    e->movement = get_movement_function(M_FUNC_MOVE_LINEAR);
    return;
}

static void move_sine(Entity *e)
{
    // An aimed sine wave, varying on a line set by the angle.
    double period_adj_time, theta = e->angle;
    uint32_t t = stage.timer - e->t;
    float amplitude = 2;
    period_adj_time = (double)t/FPS*2*PI;
    // Apply the standard rotation matrix by theta to f(t) = (t, sint), take the derivative,
    // and reverse the sign of the change in x for mystifying reasons.
    e->dy = sin(theta)*e->speed + cos(theta)*cos(period_adj_time)*amplitude;
    e->dx = cos(theta)*e->speed - sin(theta)*cos(period_adj_time)*amplitude;
    return;
}

static void move_down_sine(Entity *e)
{
    // A sine wave headed downward. Slightly fewer calculations than a regular move_sine.
    double t, period, amplitude;
    t = stage.timer - e->t;
    period = 0.5;
    amplitude = 2;
    e->dx = ENEMY_BULLET_SPEED;
    e->dy = cos((double)t/FPS*2*PI/period)*amplitude;
    return;
}

static void move_linear(Entity *e)
{
    // Do nothing: there are no changes to be made to the gradient with respect to t.
    return;
}

static void move_follow(Entity *e)
{
    // Accelerate linearly toward the parent, or just continue in the current direction if the parent is gone.
    if (!e->parent)
    {
        e->movement = get_movement_function(M_FUNC_MOVE_LINEAR);
        return;
    }
    float dx, dy;
    getGradient(e->parent->x, e->parent->y, e->x, e->y, &dx, &dy);
    e->dx += dx;
    e->dy += dy;
    return;
}

static void move_vacuum(Entity *e)
{
    // Get sucked to the parent.
    if (!e->parent)
    {
        e->movement = get_movement_function(M_FUNC_MOVE_LINEAR);
        return;
    }
    float dx, dy, angle;
    getGradient(e->parent->x, e->parent->y, e->x, e->y, &dx, &dy);
    angle = atan2(dy, dx);
    e->dx = 5 * e->speed * cos(angle);
    e->dy = 5 * e->speed * sin(angle);
    return;
}

static void move_fall(Entity *e)
{
    // Attempts to create  a nice fall, with fixed terminal velocity, for drops etc.
    // Moves downward, and has its horizontal velocity converge to 0.
    // TODO: move gravity to a global #define once settled.
    float new_dx = 0.9 * e->dx;
    if (fabs(new_dx) < 1)
        new_dx = 0;
    e->dx = new_dx;
    e->dy += DROP_SPEED/10.f;
    if (e->dy >= DROP_SPEED)
    {
        e->movement = get_movement_function(M_FUNC_MOVE_DOWN);
    }
    return;
}

//void (*get_movement_function(int param))(float*,float*,...)
m_func get_movement_function(int param)
{
    // Return a function pointer to a movement function based on the input string.
    // In OOP terms, this is a movement function factory.
    // Implemented as a simple switch for now: param is an enum for convenience.
    switch (param)
    {
        case M_FUNC_MOVE_PLAYER:
            return &move_player;
            break;
        case M_FUNC_MOVE_DOWN:
            return &move_down;
            break;
        case M_FUNC_MOVE_DIRECTION:
            return &move_direction;
            break;
        case M_FUNC_MOVE_LINEAR:
            return &move_linear;
            break;
        case M_FUNC_MOVE_SINE:
            return &move_sine;
            break;
        case M_FUNC_MOVE_VACUUM:
            return &move_vacuum;
            break;
        case M_FUNC_MOVE_FALL:
            return &move_fall;
            break;
        default:
            return &move_down;
    }
}
