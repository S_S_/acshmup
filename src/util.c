#include "util.h"

// Collision tests for internal use.
// Collision between two circles.
static int circle_collision(Entity *e1, Entity *e2)
{
    float max_dist, dx, dy;
    max_dist = e1->hitbox.radius + e2->hitbox.radius;
    dx = fabs(e1->x - e2->x);
    dy = fabs(e1->y - e2->y);
    // Ensure that the square of the hypotenuse is less than the maximum distance.
    return (dx*dx + dy*dy < max_dist*max_dist);
}

// Collision between circle e1 and polygon e2.
static int polygon_collision(Entity *e1, Entity *e2)
{
    float px, py;
    // Test vertices within the circle first.
    for (int i =0; i<e2->hitbox.n_vert; ++i)
    {
        px = fabs(e1->x - e2->hitbox.x[i] - e2->x);
        py = fabs(e1->y - e2->hitbox.y[i] - e2->y);
        if (px*px + py*py < e1->hitbox.radius * e1->hitbox.radius)
            return 1;
    }
    // Get the point on the circle's radius nearest the polygon's centre, then test for collision.
    float dx, dy, angle;
    float x_list[e2->hitbox.n_vert], y_list[e2->hitbox.n_vert]; //VLAs are scary.
    getGradient(e1->x, e1->y, e2->x, e2->y, &dx, &dy);
    angle = atan2(dy, dx);
    // Rotate some radius-length line segment by the angle.
    px = e1->hitbox.radius * cos(angle);
    py = e1->hitbox.radius * sin(angle);
    // Translate dx and dy.
    px += e1->x;
    py += e1->y;
    // Translate the polygon.
    for (int i = 0; i<e2->hitbox.n_vert; ++i)
    {
        x_list[i] = e2->hitbox.x[i] + e2->x;
        y_list[i] = e2->hitbox.y[i] + e2->y;
    }
    // Test for collision.
    return point_in_polygon(e2->hitbox.n_vert, x_list, y_list, px, py);
}

// Test collision between two entities By design, we guarantee that one of the hitboxes is a circle for simplicity.
int collision(Entity *e1, Entity *e2)
{
    int collides = 0;
    if (e1->hitbox.type == HITBOX_CIRCLE) {
        switch (e2->hitbox.type) 
        {
            case HITBOX_CIRCLE:
                collides = circle_collision(e1, e2);
                break;
            case HITBOX_POLYGON:
                collides = polygon_collision(e1, e2);
                break;
            default: // Do nothing if there is no hitbox.
                break;
        }
    }
    else
    {
        collides = polygon_collision(e2, e1);
    }
    return collides;
    //return (max(x1, x2) < min(x1 + w1, x2 + w2)) && (max(y1, y2) < min(y1+h1, y2+h2)); Rectangles.
}

// Projects a circular grazebox of radius rad about e1 and tests for collision with e2.
int graze(Entity *e1, Entity *e2, float rad)
{
    Entity tmp;
    memcpy(&tmp, e1, sizeof(Entity));
    if (tmp.hitbox.type != HITBOX_CIRCLE)
    {
        SDL_Log("Something has gone horribly wrong with grazing. Look out for memory leaks.\n");
    }
    tmp.hitbox.radius = rad;
    tmp.hitbox.type = HITBOX_CIRCLE; // Just in case.
    return collision(&tmp, e2);
}

// Calculates the normalised gradient between points x1y1 and x2y2 and writes them to *dx*dy.
void getGradient(int x1, int y1, int x2, int y2, float *dx, float *dy)
{
    int steps = max(abs(x1 - x2), abs(y1 - y2));
    if (steps == 0)
    {
        *dx = *dy = 0;
        return;
    }
    *dx = (x1 - x2);
    *dx /= steps;

    *dy = (y1 - y2);
    *dy /= steps;
}

// Circle collision - construct a circle around each rectangle and check for intersection.
int circleCollision(float x1, float y1, int w1, int h1, float s1, 
        float x2, float y2, int w2, int h2, float s2)
{
    float dx, dy, pointdist;
    dx = fabs(x1-x2);
    dy = fabs(y1-y2);
    pointdist = 0.5*(max(h1,w1)*s1 + max(h2,w2)*s2);

    // Check that the hypotenuse between centres 1 and 2 is less than pointdist.    
    return (sqrt(dx*dx + dy*dy) < pointdist);
}

// Test whether a point is in a polygon by the Jordan Curve Theorem: cast a ray right and counting intersections.
// Odd intersections means that the point is internal. Even, external.
int point_in_polygon(int n_vert, float *vert_x, float *vert_y, float point_x, float point_y)
{
    int i, j, c = 0;
    for (i = 0, j = n_vert-1; i < n_vert; j = i++)
    {
        // Test that the point is aligned with the edge on the y-axis
        // and then cast it x-ward to test for intersection.
        // This is W. Randolph Franklin's implementation.
        // Optimisation is still available by eliminating the division - see Joseph Samosky, 1993.
        if ( ((vert_y[i] > point_y) != (vert_y[j] > point_y)) && 
                point_x < ((vert_x[j]-vert_x[i]) * (point_y-vert_y[i])/(vert_y[j]-vert_y[i])+vert_x[i]) )
        {
            c = !c;
        }
    }
    return c;
}


// Maps to retrieve enums from strings,
#define X(name) #name,
char const * const m_func_name[] = { M_FUNC_LIST };
#undef X

enum movement_functions str_to_movement(char const *name)
{
    for (size_t i = 0; i < sizeof(m_func_name) / sizeof(*m_func_name); ++i)
    {
        if (strcmp(m_func_name[i], name) == 0)
            return (enum movement_functions)i;
    }
    // On failure.
    return -1;
}

#define X(name) #name,
char const * const p_func_name[] = { P_FUNC_LIST };
#undef X

enum pattern_functions str_to_pattern(const char *name)
{
    for (size_t i = 0; i < sizeof(p_func_name) / sizeof(*p_func_name); ++i)
    {
        if (strcmp(p_func_name[i], name) == 0)
            return (enum pattern_functions)i;
    }
    // On failure.
    return -1;

}

// A manual implementation of strdup, since it's not in the standard.
char *strdup(const char *str)
{
    size_t str_len = strlen(str);
    char *result = malloc(str_len + 1);
    if (result == NULL)
    {
        return NULL; // Out of memory error.
    }
    memcpy(result, str, str_len + 1);
    return result;
}


/* Hash Table Functionality */
// An implementation of hash tables, using FNV1a, drawn shamelessly from Ben Hoyt's MIT-licenced implementation.
// Inclusions for the below: Ones already in `defs.h` are commented.
#include <assert.h>
//#include <stdint.h>
//#include <stdlib.h>
//#include <string.h>

// Hash table entry: could be emtpy or full.
typedef struct ht_entry {
    const char *key;
    void *value;
} ht_entry;

struct ht {
    ht_entry *entries;
    size_t capacity;    // Size of entries array.
    size_t length;      // Number of items currently present.
};

#define INITIAL_CAPACITY 16 // Some non-zero power of two.

ht *ht_create(void)
{
    // Allocate space for the hash table.
    ht *table = malloc(sizeof(ht));
    if (table == NULL)
    {
        return NULL;
    }
    table->length = 0;
    table->capacity = INITIAL_CAPACITY;
    // Allocate zeroed space for entry buckets.
    table->entries = calloc(table->capacity, sizeof(ht_entry));
    if (table->entries == NULL){
        free(table); // Don't leak memory on error.
        return NULL;
    }
    return table;
}

void ht_destroy(ht *table) 
{
    // Free allocated keys first.
    for (size_t i = 0; i < table->capacity; ++i)
    {
        free((void*)table->entries[i].key);
    }
    // Free the entries array, then the table itself.
    free(table->entries);
    free(table);
}

// Constants for 64-bit FNV 1a.
#define FNV_OFFSET 14695981039346656037UL
#define FNV_PRIME 1099511628211UL

// Return a 64-bit FNV-1a hash for key.
static uint64_t hash_key(const char *key)
{
    uint64_t hash = FNV_OFFSET;
    for (const char *p = key; *p; ++p)
    {
        hash ^= (uint64_t)(unsigned char)(*p); // XOR the bytes of the key.
        hash *= FNV_PRIME;                     // Multiply by the 64-bit FNV prime.
    }
    return hash;
}

void *ht_get(ht *table, const char *key)
{
    uint64_t hash = hash_key(key);
    size_t index = (size_t)(hash & (uint64_t)(table->capacity - 1));

    // Loop until finding an empty entry.
    while (table->entries[index].key != NULL)
    {
        if (strcmp(key, table->entries[index].key) == 0)
        {
            return table->entries[index].value; // Found the key, return the value.
        }
        // If the key wasn't found, look to the next slot: linear probing.
        ++index;
        if (index >= table->capacity)
        {
            index = 0; // Wrap around at the end of the array.
        }
    }
    return NULL;
}

// Internal function to set an entry without expanding the table.
static const char *ht_set_entry(ht_entry *entries, size_t capacity,
        const char *key, void *value, size_t *p_length)
{
    // AND the hash with capacity-1 to ensure that it's within the array.
    uint64_t hash = hash_key(key);
    size_t index = (size_t)(hash & (uint64_t)(capacity - 1));

    // Loop until finding an empty entry, wrapping around if necessary.
    while (entries[index].key != NULL)
    {
        if (strcmp(key, entries[index].key) == 0)
        {
            // Found key (it already exists), and so update value.
            entries[index].value = value;
            return entries[index].key;
        }
        // If the key wasn't in the slot, perform linear probing.
        ++index;
        if (index >= capacity)
            index = 0;
    }
    // The key wasn't found. Allocate+copy if needed, then insert it.
    if (p_length != NULL)
    {
        key = strdup(key);
        if (key == NULL)
            return NULL;
        (*p_length)++;
    }
    entries[index].key = (char*)key;
    entries[index].value = value;
    return key;
}

// Double the size of a hash table. Return true on success, or false if out of memory.
static bool ht_expand(ht *table)
{
    // Allocate the new array of entries.
    size_t new_capacity = table->capacity * 2;
    if (new_capacity < table->capacity)
        return false; // Catch overflows, although they're undefined behaviour and this might disappear.
    ht_entry *new_entries = calloc(new_capacity, sizeof(ht_entry));
    if (new_entries == NULL)
        return false;
    // Iterate entries, moving all non-empty ones to the new table's entries.
    for (size_t i = 0; i < table->capacity; ++i)
    {
        ht_entry entry = table->entries[i];
        if (entry.key != NULL)
            ht_set_entry(new_entries, new_capacity, entry.key, entry.value, NULL);
    }
    // Free the old entries and update the table's details.
    free(table->entries);
    table->entries = new_entries;
    table->capacity = new_capacity;
    return true;
}

const char *ht_set(ht *table, const char *key, void *value)
{
    assert(value != NULL);
    if (value == NULL)
        return NULL;

    // If length would exceed half the current capacity, expand the table.
    if (table->length >= table->capacity / 2)
    {
        if (!ht_expand(table))
            return NULL;
    }
    // Set entry and update length.
    return ht_set_entry(table->entries, table->capacity, key, value,
            &table->length);
}

size_t ht_length(ht *table)
{
    return table->length;
}

hti ht_iterator (ht *table)
{
    hti it;
    it._table = table;
    it._index = 0;
    return it;
}

bool ht_next(hti *it)
{
    // Loop until the end of the entries array.
    ht *table = it->_table;
    while (it->_index < table->capacity)
    {
        size_t i = it->_index;
        it->_index++;
        if (table->entries[i].key != NULL)
        {
            // Found next non-empty item, update iterator key and value.
            ht_entry entry = table->entries[i];
            it->key = entry.key;
            it->value = entry.value;
            return true;
        }
    }
    return false;
}

