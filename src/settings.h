#pragma once

#include "defs.h"

/* General config functions.
 *
 * For init and update purposes.
 */

void init_config(void);
json_t* load_config(void);
void update_config(void);
void reset_config(void);

/* Volume controls.
 *
 * Increment or decrement the appropriate volume slider by 5%.
 * Note that this scales to percentages: the default SDL_VOLUME_MAX is 128.
 */
void inc_vol_bgm(void);
void inc_vol_sfx(void);
void dec_vol_bgm(void);
void dec_vol_sfx(void);

/* Display options
 *
 * Do things like alter FPS or fullscreen status.
 */
void toggle_fullscreen(void);
