// Strictly speaking, `main` really shouldn't have a header file.
// This just serves to collect the other headers called by it.
#include "defs.h"

#include "util.h"
#include "init.h"
#include "draw.h"
#include "input.h"
