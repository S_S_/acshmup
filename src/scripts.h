#include "defs.h"

/* Script-specific definitions et al. */


extern Stage stage;

extern void set_bgm(const char*);
extern enum movement_functions str_to_movement(const char*);
