#pragma once

#include "defs.h"

void init_menus(void);
void cleanup_menus(void);
void menu_mode(menu*);
void free_menu(menu*);
void add_menu_item(menu*, const char*, void(*), void(*), void(*), GPU_Image*, int, int);
