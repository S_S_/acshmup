#include "defs.h"
#include "replays.h"

struct replay {
    // Metadata
    char version[10];
    char filename[32];
    int stages;
    // The actual replay. One array per stage.
    int *durations;
    int **actions[P_CONTROL_MAX];
};

rpy* rpy_create()
{
    return NULL;
}

void rpy_free(rpy *replay)
{
    return;
}

void rpy_update(rpy *replay, int action[P_CONTROL_MAX])
{
    return;
}


// Create a replay file. For efficiency, this is a binary.
char rpy_save(rpy *replay)
{
    return 0;
}

rpy* rpy_load(const char *filenmae)
{
    return NULL;
}
