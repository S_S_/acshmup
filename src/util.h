// This might require an include guard.
#pragma once

#include "defs.h"
// For the hash table.
#include <stdbool.h>
#include <stddef.h>

/* Geometry */
int collision(Entity*, Entity*);
int graze(Entity*, Entity*, float);
void getGradient(int, int, int, int, float*, float*);
int circleCollision(float, float, int, int, float, float, float, int, int, float);
int point_in_polygon(int, float*, float*, float, float);

/* For X-macros */
enum movement_functions str_to_movement(const char *name);
enum pattern_functions str_to_pattern(const char *name);

// strdup: It's not a real part of string.h, but we want it on occasion regardless.
char *strdup(const char*);

/* Hash table API */
// Drawn from Ben Hoyt's implementation.
// The actual hash table. Created with `ht_create` and destroyed with `ht_destroy`.
typedef struct ht ht;

// Create a hash table and return a pointer to it. Returns NULL if out-of-memory.
ht* ht_create(void);

// Free the hash table, and everything inside it.
void ht_destroy(ht *table);

// Get item with NUL-terminated key from hash table. Return the value or NULL if not found.
void* ht_get(ht *table, const char *key);

// Set the item with the given NUL-terminated key to the provided value, which must not be NULL.
// If not already present, the key is copied to newly-allocated memory. Keys are automatically 
// freed by `ht_destroy`.
// Returns the address of the key, or NULL if out of memory.
const char* ht_set(ht *table, const char *key, void *value);

// Return the number of items in a hash table.
size_t ht_length(ht *table);

// Hash table iterator: created with `ht_iterator` and iterated with `ht_next`.
typedef struct {
    const char *key;    // Current key.
    void *value;        // Current value.

    //Internal fields. Don't use directly.
    ht *_table;         // Reference to table on which we're iterating.
    size_t _index;      // Current index into ht._entries.
} hti;

// Create a new hash table iterator.
hti ht_iterator(ht *table);

// Move an iterator to the next item, update the iterator's key
// and value to the current item, and return true. If there are
// no more items, return false.
// For thread-safety, don't call ht_set during iteration.
bool ht_next(hti *it);
