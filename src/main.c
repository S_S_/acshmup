#include "main.h"

// Useful globals.
App app;
Stage stage;
Config config;
Entity *player;
atlasImage *sprites;
GPU_Image *atlas_texture;
ht *sprite_atlas;
Mix_Chunk *sounds[SND_MAX];

uint32_t colour_shader, noise_shader, speen_shader;
GPU_ShaderBlock colour_block, noise_block, speen_block;
int colour_loc, noise_loc, speen_res_loc, speen_time_loc, speen_pos_loc, speen_rad_loc;

int main(int argc, char **argv)
{
    long then;
    float remainder;

    compile_sprite_atlas();

    memset(&app, 0, sizeof(App));
    app.scaled_coords.x = SCREEN_WIDTH;
    app.scaled_coords.y = SCREEN_HEIGHT;
    // Initialise with a window named by the param.
    init("Wings of Clay");
    // Set tidying function when closing.
    atexit(cleanup);
    
    then = SDL_GetTicks();
    remainder = 0;

    // Do things.
    int run = MODE_MENUS;
    while (run)
    {
        prepareScene();
        handleInput();
        app.delegate.logic();
        app.delegate.draw();
        presentScene();
        //SDL_Delay(16);
        capFrameRate(&then, &remainder);
    }
    // Stop doing things.
    exit(0);
}

