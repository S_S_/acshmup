/* All sorts of places include this */
#pragma once

/* Includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <jansson.h>
#include <SDL2/SDL.h>
//#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_gpu.h>

/* Actual definitions. */
#define SCREEN_WIDTH    1280
#define SCREEN_HEIGHT   720
#define BORDER_LEFT     370
#define BORDER_RIGHT    910
#define OOB_THRESHOLD   100
#define MAX_KEYBOARD_KEYS 350
#define SIDE_PLAYER 0
#define SIDE_ENEMY  1
#define GRAZE_RADIUS 25
#define FPS config.max_framerate
#define PLAYER_SPEED    4
#define BULLET_SPEED    32
#define MAX_POWER       120
#define ENEMY_BULLET_SPEED 8
#define DROP_SPEED 2
#define MAX_LINE_LENGTH 1024
#define MAX_FILENAME_LENGTH 128
#define FONT_SIZE   32
#define MAX_NAME_LENGTH 32
#define MAX_SND_CHANNELS 8

/* A useful switch for test builds. */
#define DEBUG 0

/* Portability. */
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(__NT__)
// Windows
    #define FONT_PATH "fnt\\CharisSilBold-GX2m.ttf"
#elif __linux__
// Linux
    #define FONT_PATH "fnt/CharisSilBold-GX2m.ttf"
#else
#   error "Unsupported platform"
#endif

/* For Mathematics */
#define PI 3.14159265358979323846

/* Minmax */
#define max(A,B) ((A) > (B) ? (A) : (B))
#define min(A,B) ((A) > (B) ? (B) : (A))

/* Enums */
/* Screen orientation */
enum disp_mode {
    HORIZONTAL,
    VERTICAL
};

/* Modes - menus, gameplay, etc. */
enum game_mode {
    MODE_EXIT, //This is fed into a while loop, so exit on 0.
    MODE_MENUS,
    MODE_GAME,
    MODE_REPLAY
};

/* Stages - where in the game something is. */
enum stages {
    STAGE_1 = 1 << 0,
    STAGE_2 = 1 << 1,
    STAGE_3 = 1 << 2,
    STAGE_4 = 1 << 3,
    STAGE_5 = 1 << 4,
    STAGE_6 = 1 << 5,
    STAGE_EXTRA = 1 << 6,
    STAGE_RANDOM = 1 << 7
};

/* Player Controls */
enum player_controls {
    P_CONTROL_UP,
    P_CONTROL_DOWN,
    P_CONTROL_LEFT,
    P_CONTROL_RIGHT,
    P_CONTROL_SHOOT,
    P_CONTROL_FOCUS,
    P_CONTROL_BOMB,
    P_CONTROL_BACK,
    P_CONTROL_MAX
};

/* Hitbox types for an entity. */
enum hitbox_type {
    HITBOX_NONE,
    HITBOX_CIRCLE,
    HITBOX_POLYGON,
};

/* Pickup types. */
enum pickup_type {
    PICKUP_POINT,
    PICKUP_POWER,
    PICKUP_MULT,
    PICKUP_LIFE,
    PICKUP_MAX
};

/* Ship/Equipment types. Aspirational. */
enum primary_type {
    PRIMARY_WIDE,
    PRIMARY_NARROW,
    PRIMARY_TRAIL,
    PRIMARY_MAX
};

enum focus_type {
    FOCUS_FORWARD,
    FOCUS_HOMING,
    FOCUS_FREEZE,
    FOCUS_MAX
};

enum bomb_type {
    BOMB_BASIC,
    BOMB_MAX
};

/* Sound-related. */
enum sound_channel {
    CH_ANY = -1,
    CH_PLAYER_SHOOT,
    CH_ENEMY_SHOOT,
    CH_PLAYER_DEATH,
    CH_ENEMY_HIT,
    CH_ENEMY_DEATH,
    CH_PICKUP,
    CH_POWERUP,
    CH_MAX
};

enum sound_index {
    SND_NONE = 0, // Be certain this is zero for easier defaults.
    SND_PLAYER_HIT,
    SND_ENEMY_HEALTHY,
    SND_ENEMY_DAMAGED,
    SND_PLAYER_SHOT,
    SND_ENEMY_SHOT,
    SND_DEATH_SMALL,
    SND_DEATH_LARGE,
    SND_PICKUP,
    SND_POWERUP,
    SND_MAX
};

/* Movement function types. */
// Use X-macros to allow for mapping a string to the enum's value.
#define M_FUNC_LIST \
    X(M_FUNC_MOVE_PLAYER) \
    X(M_FUNC_MOVE_DOWN) \
    X(M_FUNC_MOVE_DIRECTION) \
    X(M_FUNC_MOVE_LINEAR) \
    X(M_FUNC_MOVE_SINE) \
    X(M_FUNC_MOVE_VACUUM) \
    X(M_FUNC_MOVE_FALL) \
    X(M_FUNC_MAX)

#define X(name) name,
enum movement_functions {
    M_FUNC_LIST
};
#undef X

/* Option formation types. A type of movement, really. */
enum formation {
    FORMATION_FRONT,
    FORMATION_REAR,
    FORMATION_MAX
};

/* Pattern function types. */
#define P_FUNC_LIST \
    X(P_FUNC_SHOOT_DOWN) \
    X(P_FUNC_SHOOT_UP) \
    X(P_FUNC_SHOOT_AIMED) \
    X(P_FUNC_SHOOT_AIMED_SINE) \
    X(P_FUNC_SHOOT_STATIC_STREAM) \
    X(P_FUNC_SHOOT_TRACKING_STREAM) \
    X(P_FUNC_SHOOT_SHOTGUN) \
    X(P_FUNC_SHOOT_RING) \
    X(P_FUNC_SHOOT_WINDMILL) \
    X(P_FUNC_SHOOT_ROWS) \
    X(P_FUNC_SHOOT_SPREAD) \
    X(P_FUNC_MAX)

#define X(name) name,
enum pattern_functions {
    P_FUNC_LIST
};
#undef X

/* Getter functions, plus early typedefs of structs to let them work. */
typedef struct Entity Entity;
typedef struct Pattern Pattern;
typedef struct Option Option;
typedef void (*m_func)(Entity*);
typedef void (*p_func)(Pattern*, float, float, int);
typedef void (*f_func)(Option*, int);

/* Structs */
/* To help tidy and abstract main. */
typedef struct {
    void (*logic)(void);
    void (*draw)(void);
} Delegate;

/* Properties that an entity can have. */
// Defines a rectangular space on a sprite atlas.
typedef struct {
    char filename[MAX_FILENAME_LENGTH];
    GPU_Rect rect;
    GPU_Image *atlas_texture;
} atlasImage;

typedef struct Pattern {
    int aimed;
    int period;
    int reload;
    int speed;
    int n_shots;
    uint32_t t;
    float amplitude;
    float angle;
    p_func(shoot);
    const char *sprite;
    enum sound_index sfx;
} Pattern;

typedef struct Emitter {
    float x_offset;
    float y_offset;
    Pattern pattern;
} Emitter;

typedef struct { 
    // A hitbox is either a list of n_vert X and Y co-ordinates, or properties for a circle.
    enum hitbox_type type;
    int n_vert;
    float *x;
    float *y;
    float radius;
} Hitbox;

typedef struct {
    int point;
    int power;
    int mult;
} Drops;

/*
 * On options and formations:
 *
 * Options are a special case. They will never be real entities.
 * They have no health. They have no hitboxes. Their positions are
 * wholly determined by the parent.
 * An option is essentially a wrapper around an emitter with the 
 * bare minimum extra details to make it flashy.
 * TODO: Perhaps refactor and move non-player functionality to defs.
 */

typedef struct Option {
    float x;
    float y;
    int w;
    int h;
    uint32_t t;
    float angle;
    float scale;
    int side;
    Emitter primary, focus;
    f_func(primary_formation);
    f_func(focus_formation);
    atlasImage *primary_texture, *focus_texture;
    Entity *parent;
    struct Option *next;
} Option;

/* Entity */
typedef struct Entity {
    float x;
    float y;
    int w;
    int h;
    float dx;
    float dy;
    uint32_t t;
    float angle;
    float scale;
    int speed;
    int health;
    int reload;
    int side;
    int grazed;
    int invuln_ticks;
    Drops drops;
    Hitbox hitbox;
    //SDL_Texture *texture;
    atlasImage *texture;
    //void (*movement)(float*,float*, ...);
    m_func(movement);
    Emitter *emitter;
    struct Entity *parent;
    struct Entity *next;
} Entity;

/* A script file. */
typedef struct Script {
    char name[MAX_NAME_LENGTH];
    int n_lines;
    int curr_line;
    char **lines;
    struct Script *next; // Just in case, for more complex setups.
} Script;

/* The contents of the stage. */
typedef struct {
    Entity shipHead, *shipTail;
    Entity bulletHead, *bulletTail;
    Entity pickupHead, *pickupTail;
    uint32_t timer;
    int score;
    Script *script;
    const char *name;
    const char *desc;
} Stage;

/* Data related to an overall run. */
typedef struct {
    uint32_t score, graze;
    int lives, power;
    enum primary_type primary;
    enum focus_type focus;
    enum bomb_type bomb;
    int n_options, options_max, option_threshold;
    Option option; // Covers both focused and unfocused.
} Run;

/* Menus */
typedef struct menu_item {
    struct menu_item *prev;
    struct menu_item *next;
    void (*action)(void);
    void (*left)(void);
    void (*right)(void);
    int x;
    int y;
    char label[MAX_NAME_LENGTH];
    GPU_Image *button;
} menu_item;

typedef struct menu {
    struct menu *parent;
    menu_item menu_head;
    menu_item *menu_tail;
} menu;

/* A basic struct for co-ordinates. */
typedef struct Point {
    float x;
    float y;
} Point;

/* Bundle things for the GUI. */
typedef struct {
    //SDL_Renderer *renderer;
    SDL_Window *window;
    GPU_Target *screen;
    
    Delegate delegate;
    int controls[P_CONTROL_MAX];
    int control_state[P_CONTROL_MAX];
    int keyboard[MAX_KEYBOARD_KEYS];
    menu *active_menu;
    menu_item *selected_item;
    Mix_Music *bgm;
    Run run;

    Point scaled_coords;
} App;

/* Struct of all options, config parameters, etc. */
typedef struct {
    // Ship options as specified from the menus.
    enum primary_type primary;
    enum focus_type focus;
    enum bomb_type bomb;
    int fullscreen;
    int max_framerate;
    int vol_bgm;
    int vol_sfx;
    int controls[P_CONTROL_MAX];
} Config;
