#pragma once

#include "defs.h"

// API for creating, saving and playing replays.
typedef struct replay rpy;

// Create a replay.
rpy* rpy_create(void);

// Free a replay.
void rpy_free(rpy *replay);

// Log to a replay.
void rpy_update(rpy *replay, int action[P_CONTROL_MAX]);

// Save a replay to the disc. Returns 0 on success, 1 on error.
char rpy_save(rpy *replay);

// Load a replay with the specified filename.
rpy* rpy_load(const char *filename);
