#include "text.h"

extern App app;

static TTF_Font *font;
static SDL_Colour white = {0xff, 0xff, 0xff, 0xff};

void init_fonts(void)
{
    font = TTF_OpenFont(FONT_PATH, FONT_SIZE);
}

GPU_Image *toTexture(SDL_Surface *surface, int destroySurface)
{
    GPU_Image *texture;
    //texture = SDL_CreateTextureFromSurface(app.renderer, surface);
    texture = GPU_CopyImageFromSurface(surface);

    if (destroySurface)
        SDL_FreeSurface(surface);

    return texture;

}

GPU_Image *getTextTexture(const char *text)
{
    SDL_Surface *surface;
    surface = TTF_RenderUTF8_Blended_Wrapped(font, text, white, MAX_LINE_LENGTH);
    return toTexture(surface, 1);
}
