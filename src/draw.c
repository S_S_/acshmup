#include "draw.h"
#include "util.h"
#include <dirent.h>

extern App app;
extern Config config;
extern ht *sprite_atlas;
extern atlasImage *sprites;
extern GPU_Image *atlas_texture;

extern uint32_t colour_shader, noise_shader, speen_shader;
extern GPU_ShaderBlock colour_block, noise_block, speen_block;
extern int colour_loc, noise_loc, speen_res_loc, speen_time_loc, speen_pos_loc, speen_rad_loc;


void prepareScene(void)
{
    //SDL_SetRenderDrawColor(app.renderer, 25, 25, 25, 255);
    //SDL_RenderClear(app.renderer);
    GPU_ClearRGB(app.screen, 25, 25, 25);
}

void presentScene(void)
{
    GPU_Flip(app.screen);
}

void blit(GPU_Image *texture, int x, int y)
{
    GPU_Rect dest;
    dest.x = x;
    dest.y = y;

    dest.w = texture->w;
    dest.h = texture->h;
    GPU_BlitRect(texture, NULL, app.screen, &dest);

    //SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);
    //SDL_RenderCopy(app.renderer, texture, NULL, &dest);
}

void blitTransformed(GPU_Image *texture, int x, int y, float angle, float scale)
{
    /*
    // Takes an angle and a scale, which will be applied to the object.
    GPU_Rect dest;
    dest.x = x;
    dest.y = y;
    //SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);
    dest.w = texture->w;
    dest.h = texture->h;
    */

    GPU_BlitTransform(texture, NULL, app.screen, x, y, angle, scale, scale); // No need for scalex and scaley.
                                                                             //SDL_RenderCopyEx(app.renderer, texture, NULL, &dest, angle, NULL, SDL_FLIP_NONE);
}

GPU_Image *loadTexture(char *filename)
{
    GPU_Image *texture = NULL;
    // Log, just in case.
    SDL_LogMessage(
            SDL_LOG_CATEGORY_APPLICATION,
            SDL_LOG_PRIORITY_INFO,
            "Loading %s", filename
            );
    texture = GPU_LoadImage(filename);
    return texture;
}

// Through dark magic, attempt to stabilise at 60 FPS.
void capFrameRate(long *then, float *remainder)
{
    long wait, frameTime;
    wait = (long)(1000/FPS) + *remainder;
    *remainder -= (int)*remainder;
    frameTime = SDL_GetTicks() - *then;
    wait -= frameTime;
    if (wait < 1)
        wait = 1;
    SDL_Delay(wait);
    *remainder += fmod(1000, FPS)/FPS;
    *then = SDL_GetTicks();
}

void drawHitbox(Entity *e)
{
    SDL_Color red = {255, 0, 0, 255};
    float interlaced_verts[2*e->hitbox.n_vert];
    switch (e->hitbox.type)
    {
        case HITBOX_CIRCLE:
            GPU_Circle(app.screen, e->x, e->y, e->hitbox.radius, red);
            break;
        case HITBOX_POLYGON: // Interlace the X and Y and draw.
            //interlaced_verts = malloc(e.hitbox.n_vert * sizeof(float) * 2);
            //printf("Drawing polygon.\n");
            for (int i = 0; i < e->hitbox.n_vert; ++i)
            {
                interlaced_verts[2*i] = e->hitbox.x[i] + e->x;
                interlaced_verts[2*i + 1] = e->hitbox.y[i] + e->y;
                //printf("X: %f, Y: %f\n", interlaced_verts[2*i], interlaced_verts[2*i+1]);
            }
            GPU_Polygon(app.screen, e->hitbox.n_vert, interlaced_verts, red);
            //free(interlaced_verts);
            break;
        default:
            break;
    }
}


/* Sprite Atlas Functionality */
/* Creates a sprite atlas from all textures provided.
 * Supplies metadata so that arbitrary sprites can be loaded properly.
 * Works with SDL surfaces.
 */
typedef struct Image {
    SDL_Surface *surface;
    char *filename;
} Image;

Image *images;

typedef struct atlasNode {
    int x;
    int y;
    int w;
    int h;
    int used;
    struct atlasNode *side_child;
    struct atlasNode *down_child;
} atlasNode; 

static int image_comparator(const void *a, const void *b)
{
    // Ternary operator for image comparison, by height.
    Image *i1 = (Image*)a;
    Image *i2 = (Image*)b;
    return i2->surface->h - i1->surface->h;
}

static int count_files(const char *dir)
{
    /* Recursively traverses a directory, to locate the image files within. */
    DIR *d;
    struct dirent *ent;
    char *path;
    int i = 0;

    if ((d = opendir(dir)) != NULL)
    {
        while ((ent = readdir(d)) != NULL)
        {
            if (ent->d_type == DT_DIR)
            {
                if (ent->d_name[0] != '.')
                {
                    path = malloc(strlen(dir) + strlen(ent->d_name) + 2);
                    sprintf(path, "%s/%s", dir, ent->d_name);
                    i += count_files(path);
                    free(path);
                }
            }
            else
            {
                ++i;
            }
        }
        closedir(d);
    }
    return i;
}

static void load_sprites(int *i, const char *dir)
{
    // Do the actual loading of the sprite data into the images array.
    DIR *d;
    struct dirent *ent;
    char *path;

    if ((d = opendir(dir)) != NULL)
    {
        while ((ent = readdir(d)) != NULL)
        {
            path = malloc(strlen(dir) + strlen(ent->d_name) + 2);
            if (ent->d_type == DT_DIR)
            {
                if (ent->d_name[0] != '.')
                {
                    sprintf(path, "%s/%s", dir, ent->d_name);
                    load_sprites(i, path);
                }
            }
            else
            {
                sprintf(path, "%s/%s", dir, ent->d_name);
                images[*i].surface = GPU_LoadSurface(path);
                if (images[*i].surface)
                {
                    images[*i].filename = malloc(strlen(path) + 1);
                    strcpy(images[*i].filename, path);
                    SDL_SetSurfaceBlendMode(images[*i].surface, SDL_BLENDMODE_NONE);
                    *i = *i + 1;
                }
            }
            free(path);
        }
        closedir(d);
    }
}

static int init_sprites(void)
{
    // Create a global array of sprites, load in the data, then return its size.
    int n_images, i = 0;
    n_images = count_files("gfx");
    if (n_images > 0){
        images = calloc(n_images, sizeof(Image));
        load_sprites(&i, "gfx");
        qsort(images, i, sizeof(Image), image_comparator);
    }
    return n_images;
}

static void splitNode(atlasNode *node, int w, int h)
{
    int padding = 1;
    node->used = 1;

    node->side_child = calloc(1, sizeof(atlasNode));
    node->down_child = calloc(1, sizeof(atlasNode));

    node->side_child->x = node->x + w + padding;
    node->side_child->y = node->y;
    node->side_child->w = node->w - w - padding;
    node->side_child->h = h;

    node->down_child->x = node->x;
    node->down_child->y = node->y + h + padding;
    node->down_child->w = node->w;
    node->down_child->h = node->h - h - padding;

    return;
}

static atlasNode *findNode(atlasNode *root, int w, int h)
{
    if (root->used)
    {
        atlasNode *n = NULL;
        if ((n = findNode(root->side_child, w, h)) != NULL || (n = findNode(root->down_child, w, h)) != NULL)
            return n;
    } else if ( w <= root->w && h <= root->h) {
        splitNode(root, w, h);
        return root;
    }
    return NULL;
}

static void freeNode(atlasNode *node)
{
    // Could cause a stack overflow if the tree is too large.
    if (node == NULL)
        return;
    // Recursively free the children
    freeNode(node->side_child);
    freeNode(node->down_child);
    free(node);
    return;
}

// A weakness of SDL_Surfaces, and currently a placeholder no-op.
static void blitRotated(SDL_Surface *src, SDL_Surface *dest, int destX, int destY)
{
    return;
}

// The actual generator.
void compile_sprite_atlas(void)
{
    SDL_Log("Compiling Sprite Atlas.\n");
    atlasNode *root, *n;
    SDL_Surface *atlas;
    SDL_Rect dest;
    json_t *json_obj, *json_root = json_array();
    int n_images, rotated, w, h, rotations = 0;
    // Make the preparations.
    n_images = init_sprites();
    SDL_Log("Found %d images.\n", n_images);
    root = malloc(sizeof(atlasNode));
    root->used = 0;
    root->x = 0;
    root->y = 0;
    root->w = 1024; // Replace magic numbers by some calculated constant.
    root->h = 1024; // As above.
    atlas = SDL_CreateRGBSurface(0, 1024, 1024, 32, 0x000000ff, 0x0000ff00, 0x00ff0000, 0xff000000);
    // Create the atlas, freeing the images as required.
    for (int i = 0; i < n_images; ++i)
    {
        rotated = 0;
        w = images[i].surface->w;
        h = images[i].surface->h;
        n = findNode(root, w, h);
        if (n == NULL)
        {
            rotated = 1;
            n = findNode(root, h, w);
        }
        if (n != NULL)
        {
            if (rotated)
            {
                n->h = w;
                n->w = h;
                ++rotations;
            }
            dest.x = n->x;
            dest.y = n->y;
            dest.w = n->w;
            dest.h = n->h;
            if (!rotated)
            {
                SDL_BlitSurface(images[i].surface, NULL, atlas, &dest);
            } else {
                blitRotated(images[i].surface, atlas, dest.x, dest.y);
            }
            // Append to the JSON metadata.
            json_obj = json_object();
            json_object_set_new(json_obj, "filename", json_string(images[i].filename));
            json_object_set_new(json_obj, "x", json_integer(dest.x));
            json_object_set_new(json_obj, "y", json_integer(dest.y));
            json_object_set_new(json_obj, "w", json_integer(dest.w));
            json_object_set_new(json_obj, "h", json_integer(dest.h));
            json_object_set_new(json_obj, "rotated", json_integer(rotated));
            json_array_append(json_root, json_obj);
            json_decref(json_obj);
            printf("[%04d / %04d] %s\n", i+1, n_images, images[i].filename);
        }
        SDL_FreeSurface(images[i].surface);
        free(images[i].filename);
    }
    GPU_SaveSurface(atlas, "dat/atlas.png", GPU_FILE_PNG);
    SDL_FreeSurface(atlas);
    // Write the metadata.
    FILE *fp = fopen("dat/atlas.json", "wb");
    json_dumpf(json_root, fp, JSON_INDENT(2));
    fclose(fp);
    json_decref(json_root);
    SDL_Log("Sprite atlas compiled. Proceeding.\n");
    freeNode(root); // Recursively frees the root and all of its children.
    free(images);
}

// Functions to use the sprite atlas at runtime.
void init_atlas(void)
{
    // Initialise our hash table.
    sprite_atlas = ht_create();
    // Load the atlas texture.
    atlas_texture = loadTexture("dat/atlas.png");
    // Load the metadata.
    json_t *atlas_meta;
    json_error_t error;
    atlas_meta = json_load_file("dat/atlas.json", 0, &error);
    if (!atlas_meta || !json_is_array(atlas_meta))
    {
        SDL_Log("Something happened: %s at %s.\n", error.text, error.source);
    }
    // Construct the hash table from the data.
    size_t index;
    json_t *node = NULL;
    // Fill array of `atlasImage`s with data from the metadata. Free it, and the hashmap, at cleanup.
    sprites = calloc(json_array_size(atlas_meta), sizeof(atlasImage));
    // Jansson supports a foreach, making a macro that loops over the array.
    json_array_foreach(atlas_meta, index, node)
    {
        if (!json_is_object(node))
        {
            SDL_Log("Something went wrong with node %zu of atlas.json.\n", index);
            continue; // Complain about malformed JSON, but try to continue.
        }
        strncpy(sprites[index].filename, json_string_value(json_object_get(node, "filename")), MAX_FILENAME_LENGTH);
        sprites[index].rect.x = json_integer_value(json_object_get(node, "x"));
        sprites[index].rect.y = json_integer_value(json_object_get(node, "y"));
        sprites[index].rect.w = json_integer_value(json_object_get(node, "w"));
        sprites[index].rect.h = json_integer_value(json_object_get(node, "h"));
        sprites[index].atlas_texture = atlas_texture;
        ht_set(sprite_atlas, sprites[index].filename, &sprites[index]);
    }
    //json_decref(node); // This might be a double-free, depending on how the foreach works.
    json_decref(atlas_meta);
}

void blit_atlas_image(atlasImage *img, int x, int y, float angle, float scale)
{
    // Very basic.
    GPU_BlitTransform(img->atlas_texture, &img->rect, app.screen, x, y, angle, scale, scale);
}

/* Shader Functionality
 * As part of the rendering pipeline, contains functions to utilise the SDL_gpu shader pipeline.
 * These are based heavily off the demo provided by the library itself.
 * */

// Load a shader and prepend version/compatibility information before compiling it.
// This is apparently required for compatibility on some GPUs.
// Simpler would be an inline GPU_LoadShader (for source) or GPU_CompileShader (for strings).
uint32_t load_shader(GPU_ShaderEnum shader_type, const char *filename)
{
    SDL_RWops* rwops;
    Uint32 shader;
    char* source;
    int header_size, file_size;
    const char* header = "";
    GPU_Renderer* renderer = GPU_GetCurrentRenderer();
    
    // Open file
    rwops = SDL_RWFromFile(filename, "rb");
    if(rwops == NULL)
    {
        GPU_PushErrorCode("load_shader", GPU_ERROR_FILE_NOT_FOUND, "Shader file \"%s\" not found", filename);
        return 0;
    }
    
    // Get file size
    file_size = SDL_RWseek(rwops, 0, SEEK_END);
    SDL_RWseek(rwops, 0, SEEK_SET);
    
    // Get size from header
    if(renderer->shader_language == GPU_LANGUAGE_GLSL)
    {
        if(renderer->max_shader_version >= 120)
            header = "#version 120\n";
        else
            header = "#version 110\n";  // Maybe this is good enough?
    }
    else if(renderer->shader_language == GPU_LANGUAGE_GLSLES)
        header = "#version 100\nprecision mediump int;\nprecision mediump float;\n";
    
    header_size = strlen(header);
    
    // Allocate source buffer
    source = (char*)malloc(sizeof(char)*(header_size + file_size + 1));
    
    // Prepend header
    strcpy(source, header);
    
    // Read in source code
    SDL_RWread(rwops, source + strlen(source), 1, file_size);
    source[header_size + file_size] = '\0';
    
    // Compile the shader
    shader = GPU_CompileShader(shader_type, source);
    
    // Clean up
    free(source);
    SDL_RWclose(rwops);
    
    return shader;
}

GPU_ShaderBlock load_shader_programme(uint32_t *p, const char *vertex_shader_file, const char *fragment_shader_file)
{
    uint32_t v, f;
    // Load the shaders. Let it do its thing.
    //v = load_shader(GPU_VERTEX_SHADER, vertex_shader_file);
    v = GPU_LoadShader(GPU_VERTEX_SHADER, vertex_shader_file);
    if (!v)
        GPU_LogError("Failed to load vertex shader (%s): %s", vertex_shader_file, GPU_GetShaderMessage());
    //f = load_shader(GPU_FRAGMENT_SHADER, fragment_shader_file);
    f = GPU_LoadShader(GPU_FRAGMENT_SHADER, fragment_shader_file);
    if (!f)
        GPU_LogError("Failed to load fragment shader (%s): %s", fragment_shader_file, GPU_GetShaderMessage());
    *p = GPU_LinkShaders(v, f);
    if (!p)
    {
        GPU_ShaderBlock b = {-1, -1, -1, -1};
        GPU_LogError("Failed to link shader programme (%s + %s): %s", vertex_shader_file, fragment_shader_file, GPU_GetShaderMessage());
        return b;
    }
    GPU_ShaderBlock block = GPU_LoadShaderBlock(*p, \
            "gpu_Vertex", "gpu_TexCoord", "gpu_Colour", "gpu_ModelViewProjectionMatrix");

    return block;
}

void free_shader(uint32_t p)
{
    GPU_FreeShaderProgram(p);
    return;
}

void update_colour_shader(float r, float g, float b, float a, int colour_loc)
{
    float fcolour[4] = {r, g, b, a};
    GPU_SetUniformfv(colour_loc, 4, 1, fcolour);
    return;
}

void update_perlin_noise_shader(float iTime, int noise_loc)
{
    GPU_SetUniformf(noise_loc, iTime);
    return;
}

void update_speen_shader(float w, float h, int speen_res_loc, float t, int speen_time_loc, float x, float y, int speen_pos_loc, float rad, int speen_rad_loc)
{
    float outres[2] = {w, h};
    GPU_SetUniformfv(speen_res_loc, 2, 1, outres);
    GPU_SetUniformf(speen_time_loc, t);
    Point p = virt_coords(x, y);
    float outpos[2] = {p.x, p.y};
    GPU_SetUniformfv(speen_pos_loc, 2, 1, outpos);
    float scaled_rad =  rad * app.scaled_coords.x / SCREEN_WIDTH;
    GPU_SetUniformf(speen_rad_loc, scaled_rad);
    return;
}

void init_shaders(void)
{
    // Load the test shader.
    colour_block = load_shader_programme(&colour_shader, "shd/common.vert", "shd/color.frag");
    colour_loc = GPU_GetUniformLocation(colour_shader, "u_colour");
    noise_block = load_shader_programme(&noise_shader, "shd/common.vert", "shd/perlin.frag");
    noise_loc = GPU_GetUniformLocation(noise_shader, "u_time");
    speen_block = load_shader_programme(&speen_shader, "shd/common.vert", "shd/speen.frag");
    speen_res_loc = GPU_GetUniformLocation(speen_shader, "u_res");
    speen_time_loc = GPU_GetUniformLocation(speen_shader, "u_time");
    speen_pos_loc = GPU_GetUniformLocation(speen_shader, "u_pos");
    speen_rad_loc = GPU_GetUniformLocation(speen_shader, "u_rad");
}

void cleanup_shaders(void)
{
    free_shader(colour_shader);
    free_shader(noise_shader);
    free_shader(speen_shader);
}

// Transforms a co-ordinate pair into one scaled to the current resolution.
// Returns an x-y pair as a struct.
Point virt_coords(float x, float y)
{
    Point p = {x*app.scaled_coords.x/SCREEN_WIDTH, y*app.scaled_coords.y/SCREEN_HEIGHT};
    return(p);
}
