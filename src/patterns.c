#include "patterns.h"
#include "movements.h"
#include "util.h"
#include "sound.h"

extern Stage stage;
extern Config config;
extern Entity *player;
extern ht *sprite_atlas;

static void fire_bullet(float x, float y, float angle, int speed, int side, int move, const char *sprite, enum sound_index sfx)
{
    Entity *e;
    e = calloc(1, sizeof(Entity));

    // Add the bullet to the list.
    stage.bulletTail->next = e;
    stage.bulletTail = e;

    e->side = side;
    e->x = x;
    e->y = y;

    e->t = stage.timer;
    e->health = 1;
    e->scale = 1;
    e->texture = (atlasImage*)ht_get(sprite_atlas, sprite);
    e->w = e->texture->rect.w;
    e->h = e->texture->rect.h;
    e->angle = angle;
    e->speed = speed;
    e->movement = get_movement_function(move);
    e->hitbox.type = HITBOX_POLYGON;
    float *list_x, *list_y;
    list_x = malloc(3*sizeof(float));
    list_y = malloc(3*sizeof(float));
    list_x[0] = (-e->w*cos(e->angle) - e->h*sin(e->angle))/e->scale/2;
    list_y[0] = (-e->w*sin(e->angle) + e->h*cos(e->angle))/e->scale/2;
    list_x[1] = (-e->w*cos(e->angle) + e->h*sin(e->angle))/e->scale/2;
    list_y[1] = (-e->w*sin(e->angle) - e->h*cos(e->angle))/e->scale/2;
    list_x[2] = (e->w*cos(e->angle))/e->scale/2;
    list_y[2] = (e->w*sin(e->angle))/e->scale/2;
    e->hitbox.n_vert = 3;
    e->hitbox.x = list_x;
    e->hitbox.y = list_y;
    int ch = side == SIDE_PLAYER ? CH_PLAYER_SHOOT : CH_ENEMY_SHOOT;
    play_sfx(sfx, ch);
    return;
}

static void shoot_down(Pattern *p, float x, float y, int side)
{
    float angle = PI/2;
    uint32_t t = stage.timer - p->t;
    if (t % p->reload == 0)
        fire_bullet(x, y, angle, p->speed, side, M_FUNC_MOVE_DOWN, p->sprite, p->sfx);
    return;
}

static void shoot_up(Pattern *p, float x, float y, int side)
{
    float angle = 3*PI/2;
    uint32_t t = stage.timer - p->t;
    if (t % p->reload == 0)
        fire_bullet(x, y, angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
    return;
}

static void shoot_aimed(Pattern *p, float x, float y, int side)
{
    uint32_t t = stage.timer - p->t;
    float dx, dy;
    getGradient( player->x, player->y, x, y, &dx, &dy);
    p->angle = atan2(dy, dx);
    if (t % p->reload == 0)
        fire_bullet(x, y, p->angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
    return;
}

static void shoot_aimed_sine(Pattern *p, float x, float y, int side)
{
    uint32_t t = stage.timer - p->t;
    float dx, dy;
    getGradient(player->x, player->y, x, y, &dx, &dy);
    p->angle = atan2(dy, dx);
    if (t % p->reload == 0)
        fire_bullet(x, y, p->angle, p->speed, side, M_FUNC_MOVE_SINE, p->sprite, p->sfx);
}

static void shoot_static_stream(Pattern *p, float x, float y, int side)
{
    uint32_t t = stage.timer - p->t;
    int period_ticks = p->period * FPS;
    int shot_interval = (int) period_ticks / p->n_shots;
    if (t % (p->reload + period_ticks) < period_ticks && t % shot_interval == 0)
        fire_bullet(x, y, p->angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
    return;
}

static void shoot_tracking_stream(Pattern *p, float x, float y, int side)
{
    uint32_t t = stage.timer - p->t;
    int period_ticks = p->period * FPS;
    int shot_interval = (int) period_ticks / p->n_shots;
    float dx, dy;
    getGradient(player->x, player->y, x, y, &dx, &dy);
    p->angle = atan2(dy, dx);
    if (t % (p->reload + period_ticks) < period_ticks && t % shot_interval == 0)
        fire_bullet(x, y, p->angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
    return;
}

static void shoot_shotgun(Pattern *p, float x, float y, int side)
{
    uint32_t t = stage.timer - p->t;
    int period_ticks = p->period * FPS;
    int shot_interval = (int) period_ticks / p->n_shots;
    float variance = ((rand()%10000)/10000.f - 1)*p->amplitude * PI / 180;
    if (t % (p->reload + period_ticks) == 0) // Aim at the start of the pattern.
    {
        float dx, dy;
        getGradient(player->x, player->y, x, y, &dx, &dy);
        p->angle = atan2(dy, dx);
    }
    if (t % (p->reload + period_ticks) < period_ticks && t % shot_interval == 0)
        fire_bullet(x, y, p->angle + variance, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
    return;
}

static void shoot_ring(Pattern *p, float x, float y, int side)
{
    uint32_t t = stage.timer - p->t;
    float gap = 2 * PI / p->n_shots;
    if (t % (p->reload) == 0)
    {
        for (int i = 0; i < p->n_shots; ++i)    
        {
            fire_bullet(x, y, p->angle + i*gap, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
        }
    }
    return;
}

static void shoot_windmill(Pattern *p, float x, float y, int side)
{
    uint32_t t = stage.timer - p->t;
    float gap = PI / 2;
    float angle;
    int period_ticks = p->period * FPS;
    int shot_interval = (int) period_ticks / p->n_shots;
    if (t % (p->reload + period_ticks) < period_ticks && t % shot_interval == 0)
    {
        for (int i = 0; i < 4; ++i)
        {
            angle = p->angle + i*gap + (PI)*p->n_shots*shot_interval/t;
            fire_bullet(x, y, angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
        }
    }
}

static void shoot_rows(Pattern *p, float x, float y, int side)
{
    // Shoots n_shots evenly-spaced rows of bullets. Amplitude is abused for spacing.
    uint32_t t = stage.timer - p->t;
    float offset;
    float newx; // Shoot in a straight line for efficiency.
                // Only care about non-upward directions if this is used for non-players.
    if (t % (p->reload) == 0)
    {

        if (p->n_shots == 1) // A special case, for convenience.
        {
            fire_bullet(x, y, p->angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
            return;
        }
        offset = p->amplitude/(p->n_shots-1);
        for (int i = 0; i < p->n_shots; ++i)
        {
            newx = x + 0.5*p->amplitude - i*offset;
            fire_bullet(newx, y, p->angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
        }
    }
}

static void shoot_spread(Pattern *p, float x, float y, int side)
{
    // Shoots n_shots evenly-spread across an amplitude radian arc centred on angle.
    uint32_t t = stage.timer - p->t;
    float adj_angle;
    // Originate from the centre. Don't bother with offsets.
    if (t % (p->reload) == 0)
    {
        if (p->n_shots == 1) // A special case, for convenience.
        {
            fire_bullet(x, y, p->angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
            return;
        }
        for (int i = 0; i < p->n_shots; ++i)
        {
            adj_angle = p->angle + 0.5*p->amplitude - ((float)i/(p->n_shots-1))*(p->amplitude);
            fire_bullet(x, y, adj_angle, p->speed, side, M_FUNC_MOVE_DIRECTION, p->sprite, p->sfx);
        }
    }
}

p_func get_pattern_function(int param)
{
    // Pattern function factory. Returns a function pointer based on the enum.
    switch(param)
    {
        case P_FUNC_SHOOT_DOWN:
            return &shoot_down;
            break;
        case P_FUNC_SHOOT_UP:
            return &shoot_up;
            break;
        case P_FUNC_SHOOT_AIMED:
            return &shoot_aimed;
            break;
        case P_FUNC_SHOOT_AIMED_SINE:
            return &shoot_aimed_sine;
            break;
        case P_FUNC_SHOOT_STATIC_STREAM:
            return &shoot_static_stream;
            break;
        case P_FUNC_SHOOT_TRACKING_STREAM:
            return &shoot_tracking_stream;
            break;
        case P_FUNC_SHOOT_SHOTGUN:
            return &shoot_shotgun;
            break;
        case P_FUNC_SHOOT_RING:
            return &shoot_ring;
            break;
        case P_FUNC_SHOOT_WINDMILL:
            return &shoot_windmill;
            break;
        case P_FUNC_SHOOT_ROWS:
            return &shoot_rows;
            break;
        case P_FUNC_SHOOT_SPREAD:
            return &shoot_spread;
        default:
            return &shoot_down;
            break;
    }
}
