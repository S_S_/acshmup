#include "menus.h"
#include "text.h"
#include "draw.h"
#include "input.h"
#include "settings.h"
#include "stage.h"

extern App app;
extern Config config;
extern void initStage(const char*);

static void logic(void);
static void draw(void);

/* Globals for menus. */
menu main_menu, options_menu, controls_menu, shipbuilder_menu, pause_menu;

void add_menu_item(menu *menu, const char *label, void (*action), void (*left), void (*right), \
        GPU_Image *button, int x, int y)
{
    /* A menu item factory. */
    // Create the object.
    menu_item *m;
    m = calloc(1, sizeof(menu_item));
    // Set the vital parameters.
    strcpy(m->label, label);
    m->action = action;
    m->left = left;
    m->right = right;
    m->x = x;
    m->y = y;
    m->button = button;
    // Set the pointers for the linked list.
    menu->menu_tail->next = m;
    m->prev = menu->menu_tail;
    menu->menu_tail = m;

    return;
}

void free_menu(menu *menu)
{
    menu_item *m;
    while (menu->menu_head.next)
    {
        m = menu->menu_head.next;
        menu->menu_head.next = m->next;
        GPU_FreeImage(m->button); // Destroy the button texture.
        free(m);
    }
    menu->menu_tail = &menu->menu_head;
    // Maybe free menu too if applicable.
    return;
} 

static void do_nothing(void)
{
    return;
}

static void resume_game(void)
{
    gameplay_mode();
    return;
}

static void go_back(void)
{
    if (app.active_menu == &pause_menu)
    {
        resume_game();
    }
    else if (app.active_menu->parent != NULL)
    {
        app.active_menu = app.active_menu->parent;
        app.selected_item = app.active_menu->menu_head.next;
    }
    else
    {
        app.active_menu->menu_tail->action();
    }
    return;
}

static void goto_options(void)
{
    app.active_menu = &options_menu;
    app.selected_item = app.active_menu->menu_head.next;
    return;
}

static void goto_controls(void)
{
    app.active_menu = &controls_menu;
    app.selected_item = app.active_menu->menu_head.next;
}

static void goto_shipbuilder(void)
{
    app.active_menu = &shipbuilder_menu;
    app.selected_item = app.active_menu->menu_head.next;
}

static void close_programme(void)
{
    exit(0);
    return;
}

static void start_game(void)
{
    app.control_state[P_CONTROL_SHOOT] = 1;
    initStage("stg/demo.json");
    return;
}

/* Puts the programme in menu mode, with the passed menu active. */
void menu_mode(menu *menu)
{
    app.delegate.logic = logic;
    app.delegate.draw = draw;
    app.active_menu = menu;
    app.selected_item = app.active_menu->menu_head.next;
    return;
}

void init_menus(void)
{
    // Initialise the main menu.
    memset(&main_menu, 0, sizeof(menu));
    main_menu.menu_tail = &main_menu.menu_head;
    add_menu_item(&main_menu, "Start Game", &goto_shipbuilder, NULL, NULL, getTextTexture("Start Game"), 500, 200);
    add_menu_item(&main_menu, "Extra Stage", &do_nothing, NULL, NULL, getTextTexture("Extra Stage"), 500, 250);
    add_menu_item(&main_menu, "Practise Mode", &do_nothing, NULL, NULL, getTextTexture("Practise Mode"), 500, 300);
    add_menu_item(&main_menu, "Replays", &do_nothing, NULL, NULL, getTextTexture("Replays"), 500, 350);
    add_menu_item(&main_menu, "High Scores", &do_nothing, NULL, NULL, getTextTexture("High Scores"), 500, 400);
    add_menu_item(&main_menu, "Options", &goto_options, NULL, NULL, getTextTexture("Options"), 500, 450);
    add_menu_item(&main_menu, "Quit", &close_programme, NULL, NULL, getTextTexture("Quit"), 500, 500);
    // Initialise the options menu.
    memset(&options_menu, 0, sizeof(menu));
    options_menu.parent = &main_menu;
    options_menu.menu_tail = &options_menu.menu_head;
    add_menu_item(&options_menu, "Controls", &goto_controls, NULL, NULL, getTextTexture("Controls"), 500, 200);
    add_menu_item(&options_menu, "Fullscreen", &toggle_fullscreen, NULL, NULL, getTextTexture("Fullscreen"), 500,250);
    add_menu_item(&options_menu, "Frame Rate", &do_nothing, NULL, NULL, getTextTexture("Frame Rate"), 500, 300);
    add_menu_item(&options_menu, "BGM Volume", &do_nothing, \
            &dec_vol_bgm, &inc_vol_bgm, getTextTexture("BGM Volume"), 500, 350);
    add_menu_item(&options_menu, "SFX Volume", &do_nothing, \
            &dec_vol_sfx, &inc_vol_sfx, getTextTexture("SFX Volume"), 500, 400);
    add_menu_item(&options_menu, "Restore Defaults", &reset_config, NULL, NULL, getTextTexture("Restore Defaults"), 500, 450);
    add_menu_item(&options_menu, "Back", &go_back, NULL, NULL, getTextTexture("Back"), 500, 500);
    // Initialise the controls menu.
    memset(&controls_menu, 0, sizeof(menu));
    controls_menu.parent = &options_menu;
    controls_menu.menu_tail = &controls_menu.menu_head;
    add_menu_item(&controls_menu, "0", &do_nothing, NULL, NULL, getTextTexture("UP"), 500, 100);
    add_menu_item(&controls_menu, "1", &do_nothing, NULL, NULL, getTextTexture("DOWN"), 500, 150);
    add_menu_item(&controls_menu, "2", &do_nothing, NULL, NULL, getTextTexture("LEFT"), 500, 200);
    add_menu_item(&controls_menu, "3", &do_nothing, NULL, NULL, getTextTexture("RIGHT"), 500, 250);
    add_menu_item(&controls_menu, "4", &do_nothing, NULL, NULL, getTextTexture("SHOOT"), 500, 300);
    add_menu_item(&controls_menu, "5", &do_nothing, NULL, NULL, getTextTexture("FOCUS"), 500, 350);
    add_menu_item(&controls_menu, "6", &do_nothing, NULL, NULL, getTextTexture("BOMB"), 500, 400);
    add_menu_item(&controls_menu, "7", &do_nothing, NULL, NULL, getTextTexture("BACK"), 500, 450);
    add_menu_item(&controls_menu, "Back", &go_back, NULL, NULL, getTextTexture("Back"), 500, 550);
    // Initialise the shipbuilder menu.
    memset(&shipbuilder_menu, 0, sizeof(menu));
    shipbuilder_menu.parent = &main_menu;
    shipbuilder_menu.menu_tail = &shipbuilder_menu.menu_head;
    add_menu_item(&shipbuilder_menu, "Wide Shot", &do_nothing, NULL, NULL, getTextTexture("Wide Shot"), 500, 200);
    add_menu_item(&shipbuilder_menu, "Narrow Shot", &do_nothing, NULL, NULL, getTextTexture("Narrow Shot"), 500, 250);
    add_menu_item(&shipbuilder_menu, "Trail Shot", &do_nothing, NULL, NULL, getTextTexture("Trail Shot"), 500, 300);
    add_menu_item(&shipbuilder_menu, "Start Game", &start_game, NULL, NULL, getTextTexture("Start Game"), 500, 350);
    // Initialise the pause menu.
    memset(&pause_menu, 0, sizeof(menu));
    pause_menu.menu_tail = &pause_menu.menu_head;
    add_menu_item(&pause_menu, "Resume", &resume_game, NULL, NULL, getTextTexture("Resume"), 500, 350);
    add_menu_item(&pause_menu, "Restart", &do_nothing, NULL, NULL, getTextTexture("Restart"), 500, 400);
    add_menu_item(&pause_menu, "Return", &do_nothing, NULL, NULL, getTextTexture("Return to Menu"), 500, 450);
    add_menu_item(&pause_menu, "Quit", &close_programme, NULL, NULL, getTextTexture("Quit"), 500, 500);
    // Activate the main menu.
    menu_mode(&main_menu);
}

void cleanup_menus(void)
{
    free_menu(&main_menu);
    free_menu(&options_menu);
    free_menu(&controls_menu);
    free_menu(&shipbuilder_menu);
    free_menu(&pause_menu);
}

static void do_menu(void)
{
    handleInput();
    if (app.control_state[P_CONTROL_UP])
    {
        app.control_state[P_CONTROL_UP] = 0;
        app.selected_item = app.selected_item->prev;
        if (app.selected_item == &(app.active_menu->menu_head))
        {
            app.selected_item = app.active_menu->menu_tail;
        }
    }
    if (app.control_state[P_CONTROL_DOWN])
    {
        app.control_state[P_CONTROL_DOWN] = 0;
        app.selected_item = app.selected_item->next;
        if (app.selected_item == NULL)
        {
            app.selected_item = app.active_menu->menu_head.next;
        }
    }
    if (app.control_state[P_CONTROL_LEFT])
    {
        app.control_state[P_CONTROL_LEFT] = 0;
        if (app.selected_item->left != NULL)
            app.selected_item->left();
    }
    if (app.control_state[P_CONTROL_RIGHT])
    {
        app.control_state[P_CONTROL_RIGHT] = 0;
        if (app.selected_item->left != NULL)
            app.selected_item->right();
    }
    if (app.control_state[P_CONTROL_SHOOT])
    {
        if (app.selected_item->action != &resume_game)
            app.control_state[P_CONTROL_SHOOT] = 0;
        if (app.selected_item->action != NULL)
            app.selected_item->action();
    }
    if (app.control_state[P_CONTROL_BACK])
    {
        app.control_state[P_CONTROL_BACK] = 0;
        go_back();
    }
    if (app.control_state[P_CONTROL_BOMB])
    {
        app.control_state[P_CONTROL_BOMB] = 0;
        if (app.active_menu == &main_menu && app.selected_item != app.active_menu->menu_tail)
            app.selected_item = app.active_menu->menu_tail;
        else 
            go_back();
    }
}

static void draw_config_basic(void)
{
    // Print off the FPS cap and volumes in the appropriate places.
    GPU_Image *fps, *bgm, *sfx;
    char fps_str[4], bgm_str[4], sfx_str[4];
    snprintf(fps_str, 4, "%03d", config.max_framerate);
    snprintf(bgm_str, 4, "%03d", config.vol_bgm);
    snprintf(sfx_str, 4, "%03d", config.vol_sfx);
    fps = getTextTexture(fps_str);
    bgm = getTextTexture(bgm_str);
    sfx = getTextTexture(sfx_str);
    blit(fps, 720, 300);
    blit(bgm, 720, 350);
    blit(sfx, 720, 400);
    GPU_FreeImage(fps);
    GPU_FreeImage(bgm);
    GPU_FreeImage(sfx);
}

static void draw_keybinds(void)
{
    GPU_Image *key;
    for (int i = 0; i < P_CONTROL_MAX; ++i)
    {
        key = getTextTexture(SDL_GetKeyName(config.controls[i]));
        blit(key, 720, 50*i + 100);
        GPU_FreeImage(key);
    }
}

static void draw_controls(void)
{
    // Provide a reminder of the controls, regardless of the keybinds.
    GPU_Image *accept, *back;
    char acc_str[32], back_str[32]; // Allow for some very large button names.
    snprintf(acc_str, 32, "Accept: %s", SDL_GetKeyName(config.controls[P_CONTROL_SHOOT]));
    snprintf(back_str, 32, "Back: %s", SDL_GetKeyName(config.controls[P_CONTROL_BOMB]));
    accept = getTextTexture(acc_str);
    back = getTextTexture(back_str);
    blit(accept, 20, SCREEN_HEIGHT-100);
    blit(back, 20, SCREEN_HEIGHT-60);
    GPU_FreeImage(accept);
    GPU_FreeImage(back);
}

static void logic(void)
{
    do_menu();
}

static void draw_menu(void)
{
    menu_item *m;
    for (m = app.active_menu->menu_head.next; m != NULL ; m = m->next)
    {
        if (m == app.selected_item)
        {
            GPU_Image *t = getTextTexture("-");
            blit(t, m->x - 40, m->y);
            GPU_FreeImage(t);
        }
        blit(m->button, m->x, m->y);
    }
    // Special cases and additional menu graphics.
    if (app.active_menu == &options_menu)
        draw_config_basic();
    if (app.active_menu == &controls_menu)
        draw_keybinds();
    // Always have some instructions.
    draw_controls();
}

static void draw(void)
{
    draw_menu();
}
