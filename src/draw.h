#pragma once

#include "defs.h"

/* An API for our draw functions. */
// Basic aliases around clear and flip.
void prepareScene(void);
void presentScene(void);

// Many ways to blit.
void blit(GPU_Image *texture, int x, int y);
void blitTransformed(GPU_Image *texture, int x, int y, float angle, float scale);
void blit_atlas_image(atlasImage *img, int x, int y, float angle, float scale);

// Generic drawing utilities.
GPU_Image *loadTexture(char *filename);
void capFrameRate(long *then, float *remainder);
void drawHitbox(Entity *e);

// Sprite atlas generation.
void compile_sprite_atlas(void);    // Create a sprite atlas from /gfx.
void init_atlas(void);              // Load a sprite atlas from /dat.

// Shaders
uint32_t load_shader(GPU_ShaderEnum shader_type, const char *);
GPU_ShaderBlock load_shader_programme(uint32_t *, const char *, const char *);
void free_shader(uint32_t);
void update_colour_shader(float,float,float,float,int);
void update_perlin_noise_shader(float, int);
void update_speen_shader(float,float,int,float,int, float, float, int, float, int);
void init_shaders(void);
void cleanup_shaders(void);

// Accounting for viewports.
Point virt_coords(float, float);
