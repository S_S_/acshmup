attribute vec3 gpu_Vertex;
attribute vec2 gpu_TexCoord;
attribute vec4 gpu_Colour;
uniform mat4 gpu_ModelViewProjectionMatrix;

varying vec4 colour;
varying vec2 texCoord;

varying vec2 uv;
const vec2 madd = vec2(0.001, 0.001); // Determines the "zoom". Too large and it's basically just dots.

void main(void)
{
	colour = gpu_Colour;
	uv = gpu_Vertex.xy * madd + madd;
	texCoord = vec2(gpu_TexCoord);
	gl_Position = gpu_ModelViewProjectionMatrix * vec4(gpu_Vertex, 1.0);
}
