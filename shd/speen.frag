#version 450

#define PI 3.14159265358979323846

//in vec4 color;
in vec2 texCoord;

layout(origin_upper_left) in vec4 gl_FragCoord;

uniform sampler2D tex;
uniform float u_time;
uniform vec2 u_res;
uniform vec2 u_pos;
uniform float u_rad; // Radius.

out vec4 frag_colour;

void main()
{
    vec4 sometex = texture(tex, texCoord);
    // Initialise, scale the area to the resolution and transform the co-ordinates to the centre.
    vec2 st = gl_FragCoord.xy/(u_res.xy);
    vec2 p_pos = u_pos.xy/u_res.xy;
    vec2 pos = vec2(p_pos)-st;
    vec3 outcol = vec3(0);

    // Create our fidget spinner.
    float r = length(pos)/(u_rad/u_res.x);
    float a = atan(pos.x, pos.y);
    float f = cos(a*3. + u_time/50.);
    
    outcol = vec3(1.-smoothstep(f,f+0.02,r));
    frag_colour = vec4(outcol, 0.5) * sometex;
}

